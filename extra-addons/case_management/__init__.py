# -*- coding: utf-8 -*-
import logging
import subprocess
import sys

from .setup import install_dependencies

install_dependencies()

from . import controllers
from . import models

_logger = logging.getLogger(__name__)


def check_dependencies(cr):
    """
    Checks dependencies and outputs installed pip packages at runtime
    :param cr: Database cursor, default parameter sent by Odoo
    """
    get_packages = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze'])
    _logger.info("Packages below are installed:")
    _logger.info(get_packages.decode().split())
