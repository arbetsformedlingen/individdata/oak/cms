# -*- coding: utf-8 -*-
{
    'name': "Case Management",
    'summary': "Case management system for Arbetsförmedlingen",
    'description': "Help case workers dealing with applications for internships",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',

    # any module necessary for this one to work correctly
    'depends': ['mail'],

    # external dependencies, installation terminates if not satisfied
    'external_dependencies': {
       'python': ['jwcrypto', 'rdflib', 'solidclient', 'personnummer'],
    },

    # outputs installed packages, used for debugging only
    'pre_init_hook': 'check_dependencies',

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/templates.xml',
        'views/views.xml',
        'demo/casemanagement.employer.csv',
        'demo/casemanagement.applicant.csv',
        'demo/casemanagement.application.csv',
        'demo/casemanagement.union.csv',
        'demo/casemanagement.code.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
