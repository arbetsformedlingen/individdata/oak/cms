import base64
import datetime
import hashlib
import json
import logging
import os

import jwcrypto.jwk
import jwcrypto.jwt
import requests

from solidclient.solid_client import SolidSession, SolidAPIWrapper

from .vc import NodeVC

from odoo.http import request

IDENTITY_PROVIDER_BASE_URL = os.environ.get("IDENTITY_PROVIDER_BASE_URL", "http://localhost:4000")
POD_PROVIDER_BASE_URL = os.environ.get("POD_PROVIDER_BASE_URL", "http://localhost:4001")
SECRET_SOLID_SERVER_EMAIL = os.environ.get("SECRET_SOLID_SERVER_EMAIL", "source@example.com")
SECRET_SOLID_SERVER_PW = os.environ.get("SECRET_SOLID_SERVER_PW", "source")
SECRET_SOLID_CLIENT_ID = os.environ.get("SECRET_SOLID_CLIENT_ID", "client_credentials_client_id")
SECRET_SOLID_CLIENT_SECRET = os.environ.get("SECRET_SOLID_CLIENT_SECRET", "client_credentials_client_secret")
APP_SERVER_URL = os.environ.get("APP_SERVER_URL", "http://localhost:8069/")
NODE_VC_SCRIPT_PATH = os.environ.get("NODE_VC_SCRIPT_PATH", '/var/oak/solid-client-node-experimentation/issue-vc.js')
CUSTOM_CERT = os.environ.get("SOLID_CUSTOM_CERT")

_logger = logging.getLogger(__name__)


def subscribe_resource(env, subscription_resource, session):
    """
    Subscribe Solid Pod server resource
    :param env: Odoo environment instance
    :param subscription_resource: Solid Pod server URI to create subscription to
    :type subscription_resource: str
    :param session: Session to store state in
    :type session: dict | Session
    """
    solid_session = SolidSession(state_storage=session)

    if valid_access_token_in_session(session):
        solid_session.keypair = session['solid_key']
        solid_session.access_token = session['solid_access_token']
        _logger.info("SESSION solid key '{}'".format(session['solid_key']))
        _logger.info("SESSION solid access_token '{}'".format(session['solid_access_token']))
    elif valid_access_token_in_params(env):
        update_session_with_token_from_params(env, session)
        solid_session.keypair = session['solid_key']
        solid_session.access_token = session['solid_access_token']
        _logger.info("CONFIG PARAM solid key '{}'".format(session['solid_key']))
        _logger.info("CONFIG PARAM solid access_token '{}'".format(session['solid_access_token']))
    else:
        # Not authenticated, start the process and redirect back to this endpoint
        session['solid_subscription_resource_uri'] = subscription_resource

        # Update session even if token is invalid, other values like key, client_id and client_secret are re-used
        update_session_with_token_from_params(env, session)

        solid_client_credentials_login(env, session)
        solid_session.keypair = session['solid_key']
        solid_session.access_token = session['solid_access_token']

    # Subscribe to a resource on Solid server
    if session.get('solid_subscription_resource_uri'):
        subscription_resource = session.pop('solid_subscription_resource_uri')

    if subscription_resource:
        _logger.info("==============================================")
        _logger.info(f"Subscribing to notifications on resource '{subscription_resource}'")

        # Negotiate for WebHook protocol
        gateway_data = {
            "@context": ["https://www.w3.org/ns/solid/notification/v1"],
            "type": ["WebHookSubscription2021"],
            "features": ["state", "webhook-auth"],
        }
        try:
            # Usually /gateway
            notification_endpoint = solid_session.solid_metadata.get('notification_endpoint')
            gateway_response = solid_session.post(notification_endpoint,
                                                  json.dumps(gateway_data)).json()
            _logger.info("====================== Gateway response ========================")
            _logger.info(gateway_response)

            # Usually /subscription
            subscription_endpoint = get_endpoint_from_gateway_response(gateway_response)

            # Request new WebHook subscription
            subscription_data = {
                "@context": ["https://www.w3.org/ns/solid/notification/v1"],
                "type": "WebHookSubscription2021",
                "topic": subscription_resource,
                "target": f"{APP_SERVER_URL}casemanagement/solid-notification-webhook",
            }
            # Check if not subscribed yet - only allow a single subscription with unique digest value
            # Prepare data for Subscription object
            sub_data = dict(digest=get_digest(subscription_data))
            sub_data.update(subscription_data)
            found_subscription = env['casemanagement.subscription'] \
                .search([['digest', '=', sub_data.get('digest')]], count=True)
            if not found_subscription:
                subscription_response = solid_session.post(subscription_endpoint,
                                                           json.dumps(subscription_data))
                _logger.info("======================== Subscription response ========================")
                _logger.info(subscription_response.raw_text)

                if subscription_response.status_code != 200 and "No WebId present" in subscription_response.raw_text:
                    # Key and access_token most likely expired, start auth and redirect back to this endpoint
                    session['solid_subscription_resource_uri'] = subscription_resource
                    # Forget expired key and access_token
                    session.pop('solid_key')
                    session.pop('solid_access_token')

                    # Perform login to Solid Pod server, extract and save key and access token
                    solid_client_credentials_login(env, session)
                    return subscribe_resource(env, subscription_resource, session)

                data = {
                    'digest': sub_data.get('digest'),
                    'type': sub_data.get('type'),
                    'subscription_target': sub_data.get('topic'),
                    'unsubscribe_endpoint': subscription_response.json().get('unsubscribe_endpoint'),
                    'raw_json': json.dumps(subscription_response.json()),
                }
                env['casemanagement.subscription'].create(data)

                modal_message = "Subscribed to resource '{}'; response: '{}'".format(subscription_resource,
                                                                                     subscription_response.raw_text)
            else:
                modal_message = "Tried to subscribe to resource '{}', but Subscription " \
                                "with identical digest already exists".format(subscription_resource)
                _logger.info("==============================================")
                _logger.info(modal_message)
        except requests.exceptions.MissingSchema as e:
            modal_message = "Tried to subscribe to resource '{}', but some endpoint is not available on Solid Pod " \
                            "server. Error: \"{}\"".format(subscription_resource, e)
            _logger.error("==============================================")
            _logger.error(modal_message)
        except Exception as e:
            modal_message = "Tried to subscribe to resource '{}', but unexpected error occurred: " \
                            "'{}'".format(subscription_resource, e)
            _logger.error("==============================================")
            _logger.error(modal_message)
    else:
        modal_message = "No notification subscription resource found"
        _logger.info(modal_message)

    return modal_message


def get_endpoint_from_gateway_response(gateway_response):
    """
    Extract subscription endpoint URL from gateway response
    :param gateway_response: JSON response parsed as dict
    :type gateway_response: dict
    """
    try:
        channels = gateway_response.get('notificationChannel')
        endpoints = [c['endpoint'] for c in channels if c['type'] == 'WebHookSubscription2021']
        return endpoints[0] if endpoints else None
    except Exception as e:
        _logger.error('Error parsing gateway response', exc_info=e)
        return None


def solid_login_workaround(auth_url):
    """
    Performs Solid Pod server login without user interaction given username and password are available
    Temporary workaround until a different authentication flow is possible
    :param auth_url: URL to visit using browser
    :type auth_url: str
    """
    # Visit authentication URL to get cookies
    response = requests.get(auth_url, allow_redirects=False, verify=CUSTOM_CERT or True)
    cookies = response.cookies

    _logger.info("==================== GET auth_url response ==========================")
    _logger.info(response)
    _logger.info("==================== GET auth_url cookies ==========================")
    _logger.info(cookies)
    _logger.info("==================== GET auth_url headers ==========================")
    _logger.info(response.headers)
    _logger.info("==================== GET auth_url text ==========================")
    _logger.info(response.text)

    # Not sure if this /idp/ URL is available in provider_info?
    # idp_url = solid_session.provider_info
    # _logger.info("==================== SolidSession IDP provider info ==========================")
    # _logger.info(idp_url)

    # POST login credentials with cookies from the above call
    post_response = requests.post(IDENTITY_PROVIDER_BASE_URL + "/idp/",
                                  data=dict(email=SECRET_SOLID_SERVER_EMAIL,
                                            password=SECRET_SOLID_SERVER_PW,
                                            remember="yes"),
                                  cookies=cookies,
                                  allow_redirects=False,
                                  verify=CUSTOM_CERT or True)
    location = post_response.headers.get("Location")

    _logger.info("==================== POST response text ==========================")
    _logger.info(post_response.text)
    _logger.info("==================== POST response headers ==========================")
    _logger.info(post_response.headers)
    _logger.info(location)

    # Complete authentication after successful POST to /idp/
    location_response = requests.get(location,
                                     cookies=cookies,
                                     allow_redirects=False,
                                     verify=CUSTOM_CERT or True)
    redirect_location = location_response.headers.get("Location")

    _logger.info("==================== GET location response ==========================")
    _logger.info(location_response)
    _logger.info("==================== GET location headers ==========================")
    _logger.info(location_response.headers)
    _logger.info("==================== REDIRECT URL ==========================")
    _logger.info(redirect_location)

    # This final redirect triggers retrieval of access token from Solid Pod server
    return request.redirect(redirect_location)


def solid_client_credentials_login(env, session_storage, overwrite_existing_token=False, persist=True):
    """
    Performs Solid Pod server login using client credentials flow
    Access token to be used for authenticated requests to Solid Pod server is written to session_storage
    :param env: Odoo environment instance
    :param session_storage: Session to store token and other data to
    :type session_storage: dict
    :param overwrite_existing_token: Overwrites existing token if True
    :type overwrite_existing_token: bool
    :param persist: Save newly issued token and other related data in a file
    :type persist: bool
    """
    if not valid_access_token_in_session(session_storage) or overwrite_existing_token:
        # Generate/retrieve a key-pair
        keypair = session_storage.get('solid_key') or jwcrypto.jwk.JWK.generate(kty='EC', crv='P-256')
        key = jwcrypto.jwk.JWK.from_json(keypair) if type(keypair) == str else keypair

        solid_session = SolidSession(key, state_storage=session_storage)

        if 'solid_client_id' in session_storage and 'solid_client_secret' in session_storage:
            cred_client_id = session_storage.get('solid_client_id')
            cred_client_secret = session_storage.get('solid_client_secret')
        else:
            # Client ID and client secret are given to us, we do not have to retrieve them
            cred_client_id = SECRET_SOLID_CLIENT_ID
            cred_client_secret = SECRET_SOLID_CLIENT_SECRET
            _logger.info('============================ CLIENT CREDENTIALS LOGIN 1 ============================')

        # Retrieve Access Token
        basic_secret = base64.b64encode((cred_client_id + ':' + cred_client_secret).encode('utf-8'))
        # TODO: get rid of "verify" parameter when migrated to egendata.se domain
        token_response = requests.post(url=solid_session.provider_info['token_endpoint'],
                                       data={
                                           "grant_type": "client_credentials",
                                           "scope": "webid"
                                       },
                                       headers={
                                           'Authorization': 'Basic ' + basic_secret.decode('utf-8'),
                                           'DPoP': solid_session.make_token(key,
                                                                            solid_session.provider_info[
                                                                                'token_endpoint'],
                                                                            'POST')
                                       },
                                       allow_redirects=True,
                                       verify=CUSTOM_CERT or True)
        token_response_json = token_response.json()

        if token_response.status_code != 200:
            _logger.error('========================= CLIENT CREDENTIALS LOGIN ERROR =========================')
            _logger.error(json.dumps(token_response_json, indent=4))
            token_response.raise_for_status()

        # Save in session for future use
        token_expires_datetime = datetime.datetime.now() + datetime.timedelta(
            seconds=int(token_response_json['expires_in']) - 5)
        session_storage['solid_client_id'] = cred_client_id
        session_storage['solid_client_secret'] = cred_client_secret
        session_storage['solid_key'] = key.export()
        session_storage['solid_access_token_response'] = token_response_json
        session_storage['solid_access_token'] = token_response_json['access_token']
        session_storage['solid_token_valid_until'] = token_expires_datetime

        # Save to file for re-use
        if persist:
            write_key_and_access_token_to_params(env,
                                                 session_storage['solid_key'],
                                                 session_storage['solid_access_token'],
                                                 session_storage['solid_access_token_response'],
                                                 session_storage['solid_client_id'],
                                                 session_storage['solid_client_secret'])

        _logger.info('============================ CLIENT CREDENTIALS LOGIN 2 ============================')
        _logger.info(json.dumps(token_response_json, indent=4))


def get_digest(subscription_data):
    """
    Computes SHA256 digest of the given dict represented as string
    :param subscription_data: Subscription data sent to Solid Pod server in order to subscribe to notifications
    :type subscription_data: dict
    :rtype: str
    """
    d = hashlib.sha256()
    d.update(json.dumps(subscription_data).encode())

    return d.hexdigest()


def put_response_to_solid(env, personal_number, solid_guid):
    """
    PUT response, response link to correct place in Solid Pod server, DELETE consent-link
    :param env: Odoo environment instance
    :param personal_number: Personal number of Applicant to issue VC for
    :type personal_number: str
    :param solid_guid: GUID string used to identify Solid resources belonging to a particular response
    :type solid_guid: str
    """
    try:
        if not solid_guid or len(solid_guid) < 10:
            raise ValueError("Wrong value for parameter solid_guid, not GUID")

        # Issue VC and PUT it in Solid Pod
        issuer = NodeVC(NODE_VC_SCRIPT_PATH)
        vc = issuer.issue_vc(personal_number, solid_guid)
        jsonld = vc.get('jsonld')

        _logger.info("============================ DEMO 3 VC ============================")
        _logger.info(jsonld)

        key, access_token, raw_token, client_id, client_secret, valid_until = get_key_and_access_token_from_params(env)
        solid_session = SolidAPIWrapper(client_id=client_id,
                                        client_secret=client_secret,
                                        access_token=raw_token,
                                        keypair=key,
                                        logger=_logger)

        # PUT http://localhost:3000/user/oak/responses/response-GUID
        response_url = f"{POD_PROVIDER_BASE_URL}/user/oak/responses/response-{solid_guid}"
        resp = solid_session.put_file(response_url,
                                      json.dumps(jsonld, indent=2),
                                      'application/ld+json')

        _logger.info("============================ DEMO 4 ============================")
        _logger.info(json.dumps(jsonld, indent=2))
        _logger.info("========================================================")
        _logger.info(resp.status_code)
        _logger.info(resp.text)

        # PUT http://localhost:3000/user/oak/inbox/response-link-GUID
        # Content: `<> <https://oak.se/dataResponseUrl> <${url}>.`
        response_link = f"<> <https://oak.se/dataResponseUrl> <{response_url}>."
        response_link_url = f"{POD_PROVIDER_BASE_URL}/user/oak/inbox/response-link-{solid_guid}"
        resp_link = solid_session.put_file(response_link_url, response_link, 'text/turtle')

        _logger.info("============================ DEMO 5 ============================")
        _logger.info(resp_link)
        _logger.info(resp_link.text)

        # DELETE: http://localhost:3000/source/oak/inbox/consent-link-GUID
        delete_url = f"{POD_PROVIDER_BASE_URL}/source/oak/inbox/consent-link-{solid_guid}"
        resp_delete = solid_session.delete(delete_url)

        _logger.info("============================ DEMO 6 ============================")
        _logger.info(resp_delete)
        _logger.info(resp_delete.text)
    except Exception as e:
        _logger.error("============================ DEMO ERROR ============================")
        _logger.error(e)


def get_key_and_access_token_from_params(env):
    """
    Retrieves key and access token to be used when communicating with Solid server
    :param env: Odoo environment instance
    :rtype: (str, str, dict, str, str, datetime.datetime)
    :return: Key and access token for given user
    """
    # Default value is False regardless of param key, so we can only check one value to see if it is valid
    solid_key_param = env['ir.config_parameter'].get_param('casemanagement.solid_key')

    keypair = access_token = raw_token = client_id = client_secret = valid_until = False

    if solid_key_param:
        keypair = jwcrypto.jwk.JWK.from_json(solid_key_param)
        access_token = env['ir.config_parameter'].get_param('casemanagement.solid_access_token')
        raw_token = json.loads(env['ir.config_parameter'].get_param('casemanagement.solid_raw_token'))
        client_id = env['ir.config_parameter'].get_param('casemanagement.solid_client_id')
        client_secret = env['ir.config_parameter'].get_param('casemanagement.solid_client_secret')
        valid_until = datetime.datetime.fromtimestamp(
            int(env['ir.config_parameter'].get_param('casemanagement.solid_token_valid_until')))

    return keypair, access_token, raw_token, client_id, client_secret, valid_until


def write_key_and_access_token_to_params(env, key, access_token, raw_token, client_id, client_secret):
    """
    Writes key and access token to a file for given username
    :param env: Odoo environment instance
    :param key: JWK key to write
    :type key: str
    :param access_token: JWT access token to write
    :type access_token: str
    :param raw_token: Full response object
    :type raw_token: dict
    :param client_id: Client ID
    :type client_id: str
    :param client_secret: Client secret
    :type client_secret: str
    :rtype: bool
    """
    try:

        # decoded_access_token = jwcrypto.jwt.JWT()
        # decoded_access_token.deserialize(access_token)

        # web_id = json.loads(decoded_access_token.token.objects['payload'])['webid']
        # username = web_id.split("/")[3]

        token_expires_datetime = datetime.datetime.now() + datetime.timedelta(seconds=int(raw_token['expires_in']) - 5)
        out_to_param = dict(solid_username="arbetsformedlingen",
                            solid_key=key,
                            solid_access_token=access_token,
                            solid_raw_token=json.dumps(raw_token),
                            solid_client_id=client_id,
                            solid_client_secret=client_secret,
                            solid_token_valid_until=int(token_expires_datetime.timestamp()))
        for key in out_to_param:
            env['ir.config_parameter'].set_param(f'casemanagement.{key}', out_to_param[key])
    except Exception as e:
        _logger.error(f"Error when saving key '{key}' and access_token '{access_token}': '{e}'")
        return False

    return True


def valid_access_token_in_session(session):
    """
    Checks if Solid access token is available in session, checks if it expired (if exists)
    :param session: Session object that holds Solid access token and other related data
    :type session: dict
    """
    valid_until = session.get('solid_token_valid_until')
    return all([
        session.get('solid_key'),
        session.get('solid_access_token'),
        session.get('solid_access_token_response'),
        session.get('solid_client_id'),
        session.get('solid_client_secret'),
        valid_until and valid_until > datetime.datetime.now()])


def valid_access_token_in_params(env):
    """
    Checks if file containing Solid access token exists and whether its access token is still valid
    :param env: Odoo environment instance
    :rtype: bool
    """
    key, access_token, raw_token, client_id, client_secret, valid_until = get_key_and_access_token_from_params(env)

    # All strings means no valid token found in file
    if all([type(v) == str for v in [key, access_token, raw_token, client_id, client_secret, valid_until]]):
        return False

    return all([key, access_token, raw_token, client_id, client_secret,
                valid_until and valid_until > datetime.datetime.now()])


def update_session_with_token_from_params(env, session):
    """
    Updates session with Solid access token and other related data d
    :param env: Odoo environment instance
    :param session: Session object to update with data
    :type session: dict
    """
    key, access_token, raw_token, client_id, client_secret, valid_until = get_key_and_access_token_from_params(env)
    if all([v is False for v in [key, access_token, raw_token, client_id, client_secret, valid_until]]):
        return False
    else:
        session['solid_key'] = key
        session['solid_access_token'] = access_token
        session['solid_access_token_response'] = raw_token
        session['solid_client_id'] = client_id
        session['solid_client_secret'] = client_secret
        session['solid_token_valid_until'] = valid_until
        return True


def get_authenticated_solid_session(env, session):
    """
    Checks if valid token already exists in session or in file
    Issues a new token if it does not exist
    Returns SolidSession
    :param env: Odoo environment instance
    :param session: Session object, dict-like
    :type session: dict
    :rtype: SolidSession
    :returns: Authenticated SolidSession object to be used for simple interaction with Solid Pod server
    """
    if valid_access_token_in_session(session):
        _logger.info("SESSION solid key '{}'".format(session['solid_key']))
        _logger.info("SESSION solid access_token '{}'".format(session['solid_access_token']))
    elif valid_access_token_in_params(env):
        update_session_with_token_from_params(env, session)
        _logger.info("CONFIG PARAM solid key '{}'".format(session['solid_key']))
        _logger.info("CONFIG PARAM solid access_token '{}'".format(session['solid_access_token']))
    else:
        # Update session even if token is invalid, other values like key, client_id and client_secret are re-used
        update_session_with_token_from_params(env, session)
        solid_client_credentials_login(env, session)

    solid_session = SolidSession(keypair=session.get('solid_key'),
                                 access_token=session.get('solid_access_token'),
                                 state_storage=session)

    return solid_session
