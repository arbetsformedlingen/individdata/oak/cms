# -*- coding: utf-8 -*-
{
    'name': "Fetch Case Worker Data",
    'summary': "Fetch case worker data like office code from API",
    'description': "",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',
    'depends': [
        'case_management',
        'af_ipf'
    ],
    'data': [
        'data/cron.xml'
    ],
}
