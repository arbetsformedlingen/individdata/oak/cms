from odoo import models, fields


class Comment(models.Model):
    _name = 'casemanagement.comment'
    _description = 'Comment attached to record'
    _order = "id desc"
    _translate = True

    comment_text = fields.Text(help='Comment text')

    attachment_ids = fields.One2many('casemanagement.attachment', 'comment_id', string='Attachments')

    # Can belong to any of the below
    applicant_id = fields.Many2one('casemanagement.applicant', 'Applicant', readonly=True)
    employer_id = fields.Many2one('casemanagement.employer', 'Employer', readonly=True)
    application_id = fields.Many2one('casemanagement.application', 'Application', readonly=True)
