# Copyright 2016 ICTSTUDIO <http://www.ictstudio.eu>
# Copyright 2021 ACSONE SA/NV <https://acsone.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)
import logging

import requests

from odoo import api, models, SUPERUSER_ID, _, tools
from odoo.exceptions import AccessDenied, UserError, ValidationError
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.http import request

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = "res.users"

    @staticmethod
    def _auth_oauth_get_tokens_implicit_flow(oauth_provider, params):
        # https://openid.net/specs/openid-connect-core-1_0.html#ImplicitAuthResponse
        return params.get("access_token"), params.get("id_token")

    @staticmethod
    def _auth_oauth_get_tokens_auth_code_flow(oauth_provider, params):
        # https://openid.net/specs/openid-connect-core-1_0.html#AuthResponse
        code = params.get("code")
        # https://openid.net/specs/openid-connect-core-1_0.html#TokenRequest
        auth = None
        if oauth_provider.client_secret:
            auth = (oauth_provider.client_id, oauth_provider.client_secret)

        response = requests.post(
            oauth_provider.token_endpoint,
            data=dict(
                client_id=oauth_provider.client_id,
                grant_type="authorization_code",
                code=code,
                code_verifier=oauth_provider.code_verifier,  # PKCE
                redirect_uri=request.httprequest.url_root + "auth_oauth/signin",
            ),
            auth=auth,
            verify=oauth_provider.cert_path or True,
        )
        response.raise_for_status()
        response_json = response.json()
        _logger.debug(response_json)
        # https://openid.net/specs/openid-connect-core-1_0.html#TokenResponse
        return response_json.get("access_token"), response_json.get("id_token")

    @api.model
    def auth_oauth(self, provider, params):
        oauth_provider = self.env["auth.oauth.provider"].browse(provider)
        if oauth_provider.flow == "id_token":
            access_token, id_token = self._auth_oauth_get_tokens_implicit_flow(
                oauth_provider, params
            )
        elif oauth_provider.flow == "id_token_code":
            access_token, id_token = self._auth_oauth_get_tokens_auth_code_flow(
                oauth_provider, params
            )
        else:
            return super(ResUsers, self).auth_oauth(provider, params)
        if not access_token:
            _logger.error("No access_token in response.")
            raise AccessDenied()
        if not id_token:
            _logger.error("No id_token in response.")
            raise AccessDenied()
        validation = oauth_provider._parse_id_token(id_token, access_token)
        _logger.debug(validation)
        # required check
        if not validation.get("user_id"):
            _logger.error("user_id claim not found in id_token (after mapping).")
            raise AccessDenied()
        # retrieve and sign in user
        params["access_token"] = access_token
        login = self._auth_oauth_signin(provider, validation, params)
        if not login:
            raise AccessDenied()
        # return user credentials
        return self.env.cr.dbname, login, access_token

    @api.model
    def _auth_oauth_signin(self, provider, validation, params):
        """
        Retrieve and sign in the user corresponding to provider and validated access token
            :param provider: oauth provider id (int)
            :param validation: result of validation of access token (dict)
            :param params: oauth parameters (dict)
            :return: user login (str)
            :raise: AccessDenied if signin failed

            This method can be overridden to add alternative signin methods.
        """
        oauth_uid = validation['user_id']
        try:
            oauth_user = self.search([("oauth_uid", "=", oauth_uid), ('oauth_provider_id', '=', provider)])
            if not oauth_user:
                raise AccessDenied()
            assert len(oauth_user) == 1
            oauth_user.write({'oauth_access_token': params['access_token']})
            return oauth_user.login
        except AccessDenied as access_denied_exception:
            oauth_provider = self.env["auth.oauth.provider"].browse(provider)
            if not oauth_provider.create_user:
                _logger.debug('User creation not allowed on this OIDC provider')
                return None
            values = self._generate_signup_values(provider, validation, params)
            _logger.debug(values)
            try:
                user = self._create_user_from_template(values)
                # Assign groups
                user.write({'groups_id': [(6, 0, [self.env.ref('base.group_user').id])]})
                return user.login
            except (SignupError, UserError) as e:
                _logger.error('error when creating user', exc_info=e)
                raise access_denied_exception

    @api.constrains('password', 'oauth_uid')
    def check_no_password_with_oauth(self):
        """
        Ensure no Odoo user possesses both an SAML user ID and an Odoo password.
        Except admin who is not constrained by this rule.
        """
        if not self._allow_oauth_and_password():
            # Super admin is the only user we allow to have a local password
            if self.password and self.oauth_uid and self.id is not SUPERUSER_ID:
                raise ValidationError(_("This database disallows users to "
                                        "have both passwords and OAuth IDs. "
                                        "Errors for login %s") % self.login)

    @api.model
    def _allow_oauth_and_password(self):
        """
        Check if both OAuth and local password auth methods can coexist.
        """
        return tools.str2bool(
            self.env['ir.config_parameter'].sudo().get_param('auth_oidc.allow_oauth_uid_and_internal_password')
        )

    def _check_credentials(self, password, env):
        try:
            if self.env.user.id == SUPERUSER_ID or self._allow_oauth_and_password() or not self.env.user.oauth_uid:
                return super(ResUsers, self)._check_credentials(password, env)
            else:
                raise AccessDenied()
        except AccessDenied:
            passwd_allowed = env['interactive'] or not self.env.user._rpc_api_keys_only()
            if passwd_allowed and self.env.user.active:
                res = self.sudo().search([('id', '=', self.env.uid), ('oauth_access_token', '=', password)])
                if res:
                    return
            raise

    def _autoremove_password_if_oauth(self):
        """
        Helper to remove password if it is forbidden for OAuth users
        """
        if self._allow_oauth_and_password():
            return
        to_remove_password = self.filtered(
            lambda rec: rec.id != SUPERUSER_ID and rec.oauth_uid and rec.password
        )
        to_remove_password.write({
            'password': False
        })

    @api.model_create_multi
    def create(self, vals_list):
        result = super().create(vals_list)
        result._autoremove_password_if_oauth()
        return result
