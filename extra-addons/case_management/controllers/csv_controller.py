# -*- coding: utf-8 -*-
import base64
import datetime
import logging

from dateutil import tz

from odoo import http

import json

from ..util.email import extract_attachment_data
from ..util.serialize_exception import serialize_exception

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)


class CSVController(http.Controller, CaseManagementHelper):
    @http.route("/casemanagement/upload-employers",
                methods=["GET"],
                auth="user",
                website=True)
    def upload_csv_file_get(self, modal_message=None):
        existing_campaigns = http.request.env['casemanagement.campaign'].search([])
        latest_campaign = None
        default_date = None
        latest_import = None
        latest_import_date = None
        if existing_campaigns:
            latest_campaign = existing_campaigns[0]
            default_date = latest_campaign.date_end
            latest_import = latest_campaign.import_ids[0] if latest_campaign.import_ids else None
            if latest_import:
                import_date = latest_import.create_date
                latest_import_date = get_local_datetime(import_date).strftime('%Y-%m-%d %H:%M:%S')

        return http.request.render('case_management.upload-employer-csv',
                                   {'modal_message': modal_message,
                                    'latest_campaign': latest_campaign,
                                    'latest_import': latest_import,
                                    'latest_import_date': latest_import_date,
                                    'default_date': default_date})

    @http.route("/casemanagement/upload-employer-csv",
                methods=["POST"],
                auth="user",
                website=True)
    @serialize_exception
    def upload_csv_file_post(self, **post):
        """
        Process employer records found in CSV file, redirects to Applications list
        """
        campaign_end = post.get('campaign_end')
        if not campaign_end:
            return self.upload_csv_file_get('Please fill in campaign end date')

        parsed_campaign_end = http.request.env['casemanagement.validation'].validate_campaign_date_end(campaign_end)
        if not parsed_campaign_end:
            return self.upload_csv_file_get('Campaign end date is invalid')

        # Extract file object
        attachment_data = extract_attachment_data(post, 'csv_file')

        file_name = attachment_data.get('name', 'no_name.csv')
        csv_file_binary = attachment_data.get('binary', b'')

        try:
            # Create an import object to work with
            import_wizard = http.request.env['base_import.import'].create({
                'res_model': 'casemanagement.employer',
                'file': csv_file_binary,
                'file_type': 'text/csv',
                'file_name': file_name,
            })
            fields = ["name", "org_number", "email"]
            options = {'quoting': '"', 'separator': ',', 'has_headers': True}

            # Actual import is done here
            result = import_wizard.execute_import(
                fields,
                [],
                options,
                dryrun=False
            )
        except Exception as e:
            _logger.error(f'Error in CSV import of Employers (problem with CSV?): "{e}"')
            return self.upload_csv_file_get('File is not valid, please check it and try again.')

        import_message = 'CSV file processed! '
        created_employer_ids = result['ids']
        if created_employer_ids:
            import_message += f"{len(created_employer_ids)} Employers created. " \
                              f"Result:\n\n{json.dumps(result, indent=4)}"

        # Handle potential updates to existing Employers
        # If any of the records could not be imported, the whole transaction is rolled back, so we
        # update/import one by one in update_existing_employers()
        errors = result['messages']
        if errors:
            result_preview = import_wizard.parse_preview({
                'quoting': '"',
                'separator': ',',
                'has_headers': True,
            })

            if 'error' in result_preview:
                error = result_preview['error']
                import_message = f'Problem with file: "{error}". Please check it and try again.'
                _logger.error(import_message)
            else:
                updated_employers = process_employers(import_wizard, fields, options)
                # Both updated and created are in the list below
                created_employer_ids = [e.id for e in updated_employers]
                import_message += f"{len(updated_employers)} Employers updated/created: \n" \
                                  f"{json.dumps([f'{e.name} ({e.org_number})' for e in updated_employers], indent=4)}"
        else:
            result_preview = dict()

        _logger.info(import_message)

        # Save Import and Campaign
        existing_campaign = http.request.env['casemanagement.campaign'].search([
            ['date_end', '=', parsed_campaign_end.date()]
        ])
        # TODO: discuss update/delete/create logic for existing Campaign
        if existing_campaign:
            created_campaign = existing_campaign[0]
        else:
            created_campaign = http.request.env['casemanagement.campaign'].create({
                'date_end': parsed_campaign_end.date()})

        # Update Employers that were created to belong to this Campaign
        if created_employer_ids:
            created_employers = http.request.env['casemanagement.employer'].search([['id', 'in', created_employer_ids]])
            created_employers.update({'campaign_id': created_campaign.id})

        # Save Import for reference/history/debugging
        http.request.env['casemanagement.import'].create({
            'file_name': file_name,
            'csv_file': base64.b64encode(csv_file_binary),
            'import_result': json.dumps(dict(result=result, preview=result_preview), indent=4),
            'campaign_id': created_campaign.id
        })

        # self.set_flash_in_session(import_message)

        # Possible to redirect to existing Actions passing action ID as action parameter
        # return request.redirect("/web#action=case_management.show_employers")
        return self.upload_csv_file_get(import_message)


def get_local_datetime(utc_dt_object):
    """
    Converts from UTC to local Stockholm timezone
    :param utc_dt_object: Datetime object to convert
    :type utc_dt_object: datetime.datetime
    :rtype: datetime.datetime
    """
    # TODO: Get rid of this conversion and save in correct timezone from the beginning?
    # Represent in local timezone, stored as naive UTC
    from_z = tz.tzutc()
    to_z = tz.gettz('Europe/Stockholm')
    # Tell the datetime object that it's in UTC, convert timezone, format
    return utc_dt_object.replace(tzinfo=from_z).astimezone(to_z)


def process_employers(import_wizard, fields, options):
    """
    Updates existing Employer records with new data from CSV file that failed to create new records
    Creates new Employer records if they did not exist before
    (update is not supported with CSV import out of the box)
    :param import_wizard: Import wizard used to process CSV imports (instance of 'base_import.import' model)
    :param fields: CSV import fields
    :type fields: list
    :param options: CSV import options
    :type options: dict
    :return: Updated Employer recordset
    """
    updated_employers = list()

    try:
        # Use built-in odoo tools for CSV import
        input_file_data, import_fields = import_wizard._convert_import_data(fields, options)
        import_fields, data = import_wizard._handle_multi_mapping(import_fields, input_file_data)

        parsed_employers = convert_data_to_employers(import_fields, data)
        for e in parsed_employers:
            # Find existing record by org_number
            domain = [('org_number', '=', e['org_number'])]
            existing_employer = http.request.env['casemanagement.employer'].search(domain)
            if existing_employer:
                # Update existing record with new data e
                existing_employer[0].update(e)
                updated_employers.append(existing_employer[0])
            else:
                # Create new ones
                created_employer = http.request.env['casemanagement.employer'].create(e)
                updated_employers.append(created_employer)
    except Exception as e:
        _logger.error(f'Error when updating/creating new Employers from CSV import: "{e}"')

    return updated_employers


def convert_data_to_employers(import_fields, data):
    """
    Converts parsed data to list of Employer dicts
    :param import_fields: Import fields in the same order as corresponding data
    :type import_fields: list
    :param data: Data in the same order as import_fields
    :type data: list[list]

    :return: List of dicts that can be used to update existing Employer records
    :rtype: list
    """
    parsed_employers = list()

    _logger.debug('========================= convert_data_to_employers ==========================')
    _logger.debug(json.dumps(import_fields, indent=4))
    _logger.debug(json.dumps(data, indent=4))
    _logger.debug('==============================================================================')

    if import_fields and data:
        for r in data:
            employer = dict()
            for i in range(len(import_fields)):
                # Make sure only name/org_number/email columns are imported - only these are allowed in CSV import
                if import_fields[i] in ['name', 'org_number', 'email']:
                    employer[import_fields[i]] = r[i]

            # Mark Employer to be examined because of possible update
            employer['approval'] = 'examine'

            parsed_employers.append(employer)

    _logger.debug(json.dumps(parsed_employers, indent=4))

    return parsed_employers
