import logging

from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)


class Application(models.Model):
    _name = 'casemanagement.application'
    _description = 'Application for internship by Applicant connected to Employer'
    _inherit = ['mail.thread']
    _order = "id desc"
    _translate = True

    name = fields.Char(help='Internship name')
    start_date = fields.Date(help='Start date')
    af_office = fields.Char(help='Public Employment Service local office')
    case_worker = fields.Char(help='Identifier of case worker handling this application')

    employer_id = fields.Many2one('casemanagement.employer', 'Employer')
    applicant_id = fields.Many2one('casemanagement.applicant', 'Applicant')

    applicant_registration_date = fields.Date(related='applicant_id.registration_date')
    applicant_residence_permit_type = fields.Selection(related='applicant_id.residence_permit_type')
    applicant_origin_country = fields.Selection(related='applicant_id.origin_country')

    auto_suggestion_approval = fields.Selection(
        compute='_suggest_approval',
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
        ],
        help='Eligible/Examine - automatically generated value based on Employer and Applicant property values',
        readonly=True
    )

    approval = fields.Selection(
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
            ('rejected', 'Rejected'),
            ('--', '--'),
        ],
        default='--',
        help='Eligible/Examine/-- - depending on whether Employer and Applicant were handled by case worker'
    )

    application_status = fields.Selection(
        selection=[
            ('waiting', 'Waiting'),
            ('in_progress', 'In Progress'),
            ('locked', 'Locked'),
            ('archived', 'Archived'),
        ],
        default='waiting',
        help='Waiting/In Progress/Locked/Archived - '
             'depending on whether case worker did any work on this application'
    )

    comment_ids = fields.One2many('casemanagement.comment', 'application_id', string='Comments')

    closed = fields.Boolean(
        compute='_compute_closed',
        default=False,
        help='Used to hide processed applications by default'
    )

    @api.depends('employer_id.approval', 'applicant_id.approval')
    def _suggest_approval(self):
        """
        Computes automatic approval suggestion based on property values supplied in @api.depends decorator
        """
        for record in self:
            # TODO: adjust & discuss logic
            # TODO: check that Applicant.country_id is outside EU
            if record.employer_id.approval == 'approved' and record.applicant_id.approval == 'eligible':
                record.auto_suggestion_approval = 'eligible'
            else:
                record.auto_suggestion_approval = 'examine'

    @api.depends('approval')
    def _compute_closed(self):
        """
        Computes value based on property values supplied in @api.depends decorator
        """
        for record in self:
            # TODO: adjust & discuss logic
            record.closed = record.approval in ['eligible', 'rejected']

    def mark_approved(self, *args):
        """
        Necessary logic for approving Application
        """
        for record in self:
            record.approval = 'eligible'

    def mark_rejected(self, *args):
        """
        Necessary logic for rejecting Application
        """
        for record in self:
            record.approval = 'rejected'

    def write(self, values):
        to_return = list()
        for rec in self:
            old_values = {}
            for key in values:
                old_values[key] = getattr(rec, key)
            res = super(Application, rec).write(values)
            to_return.append(res)
            body = []
            for key in values:
                field_name = self.env['ir.translation'].get_field_string(rec._name)[key]
                body.append(_("'%(field_name)s' field was changed from '%(old_value)s' to '%(new_value)s'",
                              field_name=field_name,
                              old_value=old_values[key],
                              new_value=values[key]))
            body = "\n".join(body)
            rec.message_post(body=_(body))
            _logger.info(f"{body} for {rec._name} {rec.id}")
        return to_return
