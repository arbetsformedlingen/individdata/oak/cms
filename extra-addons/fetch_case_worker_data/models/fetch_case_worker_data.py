import logging

from odoo import models, api

_logger = logging.getLogger(__name__)


class ResUser(models.Model):
    _inherit = 'res.users'

    @api.model
    def fetch_case_worker_data(self, *args):
        """
        Fetches office code from API for recordset, active_ids or all
        """

        case_worker = self.env.ref("af_ipf.ipf_endpoint_case_worker").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Filter out records without office_code
        domain = [('office_code', '=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['res.users'].search(domain) if not len(self) else self

        _logger.info("Getting Data via API for all case workers")
        case_worker_details = self._transform_data(case_worker.call())

        for record in records_to_update:
            if record.login:
                if record.login in case_worker_details and case_worker_details[record.login]['officeCode']:
                    _logger.info("Updating Case Worker office code via API for login %s" % record.login)
                    record.write({'office_code': case_worker_details[record.login]['officeCode']})
            else:
                _logger.error("Tried to update office_code on res.users record (%s) without login" % record)

    @api.model
    def _transform_data(self, record_list):
        """
        Transform list of dicts into dict of dicts with login as key and data dict as value
        :param record_list: List of dicts describing case worker records
        :type record_list: list
        :rtype: dict
        """
        try:
            return {r['userName']: r for r in record_list}
        except TypeError as e:
            _logger.error('Problem parsing user records - record_list is not list of dicts?', exc_info=e)
            _logger.error(record_list)
        except Exception as e:
            _logger.error('Problem parsing user records', exc_info=e)
            _logger.error(record_list)

        return {}
