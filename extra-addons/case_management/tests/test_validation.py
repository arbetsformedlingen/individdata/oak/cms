import logging

from odoo.tests import tagged, common

_logger = logging.getLogger(__name__)


@tagged('casemanagement')
class TestEmailSending(common.TransactionCase):

    def test_valid_email(self):
        self.assertTrue(self.env['casemanagement.validation'].valid_email('m@gmail.com'))
        self.assertTrue(self.env['casemanagement.validation'].valid_email('maaaaaa.adsfadsfa@bbc.co.uk'))
        self.assertTrue(self.env['casemanagement.validation'].valid_email('agent.smith@gov.bbc.co.uk'))
        self.assertTrue(self.env['casemanagement.validation'].valid_email('agent+smith@gov.bbc.co.uk'))
        self.assertTrue(self.env['casemanagement.validation'].valid_email('agent%smith@gov.bbc.co.uk'))
        self.assertTrue(self.env['casemanagement.validation'].valid_email('agent_smith@gov.bbc.co.uk'))

        self.assertFalse(self.env['casemanagement.validation'].valid_email('.smith@gov.bbc.co.uk'))
        self.assertFalse(self.env['casemanagement.validation'].valid_email('hello-there.yahoo.com'))
        self.assertFalse(self.env['casemanagement.validation'].valid_email('@hello-there.yahoo.com'))
        self.assertFalse(self.env['casemanagement.validation'].valid_email('hello-there@com'))
        self.assertFalse(self.env['casemanagement.validation'].valid_email('hello'))
        self.assertFalse(self.env['casemanagement.validation'].valid_email(''))
        self.assertFalse(self.env['casemanagement.validation'].valid_email(None))
        _logger.info('====================== TEST valid_email PASSED ======================')

    def test_valid_personal_number(self):
        self.assertTrue(self.env['casemanagement.validation'].valid_personal_number('199002222397'))
        self.assertTrue(self.env['casemanagement.validation'].valid_personal_number('19900222-2397'))
        self.assertTrue(self.env['casemanagement.validation'].valid_personal_number('900222-2397'))
        self.assertFalse(self.env['casemanagement.validation'].valid_personal_number('199002212397'))
        self.assertFalse(self.env['casemanagement.validation'].valid_personal_number('19900221-2397'))
        self.assertFalse(self.env['casemanagement.validation'].valid_personal_number('900221-2397'))
        _logger.info('====================== TEST valid_personal_number PASSED ======================')
