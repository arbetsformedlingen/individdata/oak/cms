# -*- coding: utf-8 -*-
import logging
import os

from odoo.tools import date_utils

from werkzeug import Response

from odoo import http
from odoo.http import request

import json

from solidclient.solid_client import SolidAPIWrapper

from ..util.rdf import collect_rdf_graph_data
from ..util.serialize_exception import serialize_exception
from ..util.email import send_email_with_attachment
from ..util.solid import solid_client_credentials_login, get_authenticated_solid_session

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)

SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3000")
DATA_FILES = ["bolagsverket.ttl", "migrationsverket.ttl", "skatteverket.ttl"]


class Applicant(http.Controller, CaseManagementHelper):
    @http.route("/casemanagement/fetch-applicant-data", auth="user", website=True)
    def fetch_applicant_data(self, username, **post):
        _logger.info(f"Username '{username}' fetching data")

        # What do we do here?
        # 1. Load private key and access token from file for the sink user
        # 2. Try and grab data from [
        # SOLID_SERVER_URL/{username}/share/migrationsverket.ttl
        # SOLID_SERVER_URL/{username}/share/bolagsverket.ttl
        # SOLID_SERVER_URL/{username}/share/skatteverket.ttl
        # ]
        # and put it into our db

        solid_session = get_authenticated_solid_session(http.request.env, request.session)

        collected_data = {}

        for file in DATA_FILES:
            url = f"{SOLID_SERVER_URL}/{username}/share/{file}"
            resp = solid_session.get(url)
            _logger.info("url '{}' status code: {}".format(url, resp.status_code))

            if resp.status_code == 200:
                # rdf_graph = resp.get_graph()
                _logger.info("OLD response:")
                _logger.info(resp.raw_text)

                collect_rdf_graph_data(collected_data, resp.raw_text)
            elif resp.status_code == 401:
                solid_client_credentials_login(request.session, overwrite_existing_token=True)
                if 'solid_access_token_response' in request.session:
                    return self.fetch_applicant_data(username, **post)
            elif resp.status_code == 404:
                _logger.info(f"Resource '{url}' not found (404)")

            if 'solid_access_token_response' in request.session:
                new_solid_session = SolidAPIWrapper(client_id=request.session.get('solid_client_id'),
                                                    client_secret=request.session.get('solid_client_secret'),
                                                    access_token=request.session.get('solid_access_token_response'),
                                                    keypair=request.session.get('solid_key'),
                                                    logger=_logger)
                new_resp = new_solid_session.get(url)
                _logger.info("NEW response:")
                _logger.info(new_resp.text)

        _logger.info(f"collected_data {collected_data}")

        modal_message = "No personal_number or name in collected_data"
        # Create an applicant in the db based on the variables.
        # In order to create an applicant we need at least a personal number and a name
        if "personal_number" in collected_data and "first_name" in collected_data and "last_name" in collected_data:
            try:
                # TODO: prepare data before sending to Applicant
                collected_data.pop("employer_good")
                collected_data.pop("employer")
                collected_data.pop("residence_permit_type")

                # TODO: Normalize personal number? Is it always in the same format?
                applicant = http.request.env["casemanagement.applicant"].search(
                    [['personal_number', '=', collected_data.get('personal_number')]]
                )
                if applicant:
                    # Update existing applicant
                    applicant[0].update(collected_data)
                    _logger.info(f"Updated applicant with data {json.dumps(collected_data, indent=4)}")
                    modal_message = "Updated Applicant '{} {}' ({})".format(collected_data["first_name"],
                                                                            collected_data["last_name"],
                                                                            collected_data["personal_number"])
                else:
                    # Create new applicant
                    http.request.env["casemanagement.applicant"].create(collected_data)
                    _logger.info(f"Created applicant with data {json.dumps(collected_data, indent=4)}")
                    modal_message = "Created Applicant '{} {}' ({})".format(collected_data["first_name"],
                                                                            collected_data["last_name"],
                                                                            collected_data["personal_number"])
            except Exception as e:
                _logger.error(e)
                modal_message = str(e)

        self.set_flash_in_session(modal_message)

        return self.index()

    @http.route('/casemanagement/applicant/email/<int:applicant_id>',
                methods=['POST'],
                auth='user',
                type='json')
    @serialize_exception
    def email_applicant(self, applicant_id):
        """
        Send email to a given Applicant
        :param applicant_id: Applicant ID to send email to
        :type applicant_id: int
        """
        response_json = dict(status="OK")
        try:
            fetched_applicant = http.request.env['casemanagement.applicant'].browse([applicant_id])
            json_body = http.request.jsonrequest
            if not fetched_applicant:
                response_json = dict(status="error", error=f"No Applicant with ID '{applicant_id}'")
            elif not json_body:
                response_json = dict(status="error", error="Request body cannot be empty")
            elif 'content' not in json_body:
                response_json = dict(status="error", error="Request body should include 'content'")
            else:
                # Mark Applicant approval as 'examine'
                fetched_applicant.update({'approval': 'examine'})
                # Check if there is an attachment, send email
                send_email_with_attachment(fetched_applicant, json_body)
        except Exception as e:
            _logger.error(f"Error when rejecting Application by ID '{applicant_id}': '{e}'")
            response_json = dict(status="error", error=str(e))

        return response_json

    @http.route('/casemanagement/applicant/email/<int:applicant_id>',
                methods=['POST'],
                auth='user',
                website=True)
    @serialize_exception
    def email_applicant_with_attachment(self, applicant_id, **post):
        """
        Send email to a given Applicant. Expected fields are 'subject', 'content' and 'attachment' (optional)
        :param applicant_id: Applicant ID to send email to
        :type applicant_id: int
        """
        response_json = dict(status="OK")
        try:
            fetched_applicant = http.request.env['casemanagement.applicant'].browse([applicant_id])
            if not fetched_applicant:
                response_json = dict(status="error", error=f"No Applicant with ID '{applicant_id}'")
            else:
                # Mark Applicant approval as 'examine'
                fetched_applicant.update({'approval': 'examine'})
                # Check if there is an attachment, send email
                send_email_with_attachment(fetched_applicant, post, http.request.env)
        except Exception as e:
            _logger.error(f"Error when emailing Applicant ID '{applicant_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @staticmethod
    def _not_key_and_applicant_exists(key, applicant_id, json_data):
        """
        returns Response with error status code if key or applicant_id doesn't exist
        otherwise returns False.
        """
        if key not in json_data:
            return Response("", status=400)

        applicant = http.request.env["casemanagement.applicant"].browse(applicant_id)

        if not applicant:
            return Response("", content_type="application/json", status=404)

        return False

    @http.route("/casemanagement/applicant/<int:applicant_id>/set_country",
                methods=["PUT"],
                auth="user",
                type="json")
    def set_applicant_country(self, applicant_id):
        json_data = request.jsonrequest

        response = self._not_key_and_applicant_exists("country", applicant_id, json_data)
        if not response:

            try:
                applicant = http.request.env["casemanagement.applicant"].browse([applicant_id])
                applicant.write({"origin_country": json_data["country"]})
                http.request.env["attachment.helper"].attach_document(applicant.application_ids, None, None,
                                                                      "Ändrade ursprungsland: '%s'" % json_data[
                                                                          "country"])
                return applicant.read()
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @http.route("/casemanagement/applicant/<int:applicant_id>/set_residence_permit_type",
                methods=["PUT"],
                auth="user",
                type="json")
    def set_applicant_residence_permit_type(self, applicant_id):
        json_data = request.jsonrequest
        response = self._not_key_and_applicant_exists("type", applicant_id, json_data)
        if not response:
            try:
                applicant = http.request.env["casemanagement.applicant"].browse([applicant_id])
                applicant.write({"residence_permit_type": json_data["type"]})
                http.request.env["attachment.helper"].attach_document(applicant.application_ids, None, None,
                                                                      "Ändrade uppehållstillstånd: '%s'" % json_data[
                                                                          "type"])
                return applicant.read()
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @http.route("/casemanagement/applicant/<int:applicant_id>/note",
                methods=["PUT"],
                auth="user",
                type="json")
    def save_applicant_note(self, applicant_id):
        json_data = request.jsonrequest

        response = self._not_key_and_applicant_exists("note", applicant_id, json_data)
        if not response:
            try:
                applicant = http.request.env["casemanagement.applicant"].browse([applicant_id])
                http.request.env["attachment.helper"].attach_document(applicant.application_ids, None, None,
                                                                      "Anteckning: '%s'" % json_data["note"])
                return applicant.read()
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @http.route("/casemanagement/applicant/",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_all_applicants(self):

        try:
            applicants = http.request.env["casemanagement.applicant"].search([]).read()

            # Fetch in country names as well
            for applicant in applicants:

                # Join in applications
                application = http.request.env["casemanagement.application"].search([
                    ("applicant_id", "=", applicant["id"])
                ]).read()

                if len(application) > 0:
                    applicant["application"] = application[0]
                else:
                    applicant["application"] = {}

                # applicant["origin_country"] = applicant["country_id"][1]

            json_result = json.dumps(applicants, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")

        except Exception as e:
            _logger.error("Error '%s' when fetching applicants" % e)
            response = Response("[]", content_type="application/json")

        return response
