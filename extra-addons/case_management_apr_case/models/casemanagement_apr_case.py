import logging

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class CaseManagementAprCase(models.Model):
    _name = 'casemanagement.apr_case'
    _description = 'APR representation POSTed to AIS Arendehantering API'

    part_time_reason = fields.Selection(string="Part time reason",
                                        selection=[
                                            ("Okänd", "Unknown"),
                                            ("Arbete", "Work"),
                                            ("NedsättningAvArbetsförmåga", "Impaired working capacity"),
                                            ("Föräldraledig", "Parental leave"),
                                            ("AnnatGodkäntHinder", "Other"),
                                            ("Utbildningskontrakt", "Training contract")
                                        ]
                                        )

    internship = fields.Many2one(string="Internship", comodel_name="casemanagement.internship", required=True)
    type_of_internship = fields.Selection(string="Type of internship",
                                          selection=[
                                              ("Yrkesbedömning", "Work assessment"),
                                              ("Praktikantprogrammet", "Internship program")
                                          ],
                                          default="Praktikantprogrammet"
                                          )
    # TODO: Where does case_number come from? Is it the same for all Jobbsprånget internships?
    case_number = fields.Integer(string="Case number", help="RamprogramArendenummer")

    union_exists = fields.Boolean("Finns arbetstagarorganisation", default=True)
    union_approval_exists = fields.Boolean("Finns yttrande från arbetstagarorganisation",
                                           compute='_compute_union_approval_exists')

    # TODO: Add header "Planned break" to view
    planned_break_start_date = fields.Date(string="Planned break start date")
    planned_break_end_date = fields.Date(string="Planned break end date")

    application = fields.Many2one(string="Application", comodel_name="casemanagement.application", required=True)

    # TODO: find out for sure if both of these will be the logged in user
    responsible_officer = fields.Many2one(
        string="Responsible officer",
        comodel_name="res.users",
        required=True,
        readonly=True,
        default=lambda self: self.env.user)
    deciding_officer = fields.Many2one(
        string="Deciding officer",
        comodel_name="res.users",
        required=True,
        readonly=True,
        default=lambda self: self.env.user)

    decision_date = fields.Date(string="Decision date", required=True)

    # TODO: Add header "Decision period" to view
    decision_period_start_date = fields.Date(string="Start date", required=True)
    decision_period_end_date = fields.Date(string="End date", required=True)

    previous_case_number = fields.Integer(string="Previous case number")

    # Omfattning
    work_hours_percentage = fields.Integer(string="Work hours percentage", help="Between 100 and 0")

    postings = fields.One2many(string="Postings", comodel_name="casemanagement.apr_case.posting",
                               inverse_name="case_id")

    @api.constrains('work_hours_percentage')
    def _constrain_work_hours_percentage(self):
        for rec in self:
            if rec.work_hours_percentage > 100 or rec.work_hours_percentage < 0:
                raise ValidationError(_("Enter a valid percentage from 0 to 100"))

    @api.depends('application.employer_id.union_approved')
    def _compute_union_approval_exists(self):
        """
        Checks whether application.employer_id.union_approved == 'ok'
        """
        for record in self:
            record.union_approval_exists = record.application.employer_id.union_approved == 'ok'


class CaseManagementAprCasePosting(models.Model):
    _name = 'casemanagement.apr_case.posting'
    _description = 'Corresponds to Kontering in AIS API'

    case_id = fields.Many2one(string="Case", comodel_name="casemanagement.apr_case")
    account_code = fields.Char(string="Account code", required=True)
    business_code = fields.Char(string="Business code", required=True)
    financing_code = fields.Char(string="Financing code", required=True)
    cost_center_code = fields.Char(string="Cost centre code", required=True)
    project_code = fields.Char(string="Project code")
    free_classification_code = fields.Char(string="Free classification code")

    @api.constrains('account_code', 'business_code', 'financing_code', 'cost_center_code', 'project_code',
                    'free_classification_code')
    def _constrain_codes(self):
        for rec in self:
            fields_to_check = (
                'account_code',
                'business_code',
                'financing_code',
                'cost_center_code',
                'project_code',
                'free_classification_code'
            )
            for field in fields_to_check:
                if not self.env["casemanagement.validation"].valid_posting_code(rec[field]):
                    field_name = self.env['ir.translation'].get_field_string(rec._name)[field]
                    raise ValidationError(_("Invalid %(field_name)s",
                                            field_name_part_1=field_name))


class CaseManagementInternship(models.Model):
    _name = 'casemanagement.internship'
    _description = 'Holds some details specific for internship'

    address_id = fields.Many2one(string="Visitation address", comodel_name="casemanagement.address", required=True)
    place = fields.Char(string="Place", required=True)
    contact_person = fields.Many2one(string="Contact person", comodel_name="casemanagement.person", required=True)
    working_hours_description = fields.Char(string="Working hours description", required=True)
    purpose = fields.Char(string="Purpose", required=True)
    information = fields.Char(string="Information", required=True)
    work_group_code = fields.Many2one(string="Work group code", comodel_name="res.ssyk", required=True)

    @api.constrains('information')
    def _constrain_information(self):
        for rec in self:
            if len(rec.information) > 120:
                raise ValidationError(_("Information is too long"))

    @api.constrains('place')
    def _constrain_place(self):
        for rec in self:
            if len(rec.place) > 50:
                raise ValidationError(_("Place name is too long"))

    @api.constrains('working_hours_description')
    def _constrain_working_hours_description(self):
        for rec in self:
            if len(rec.working_hours_description) > 20:
                raise ValidationError(_("Working hours description too long"))

    @api.constrains('purpose')
    def _constrain_purpose(self):
        for rec in self:
            if len(rec.purpose) > 120:
                raise ValidationError(_("Purpose is too long"))


class CaseManagementApplication(models.Model):
    _inherit = 'casemanagement.application'

    apr_case_ids = fields.One2many('casemanagement.apr_case', 'application', string='APR decisions')
