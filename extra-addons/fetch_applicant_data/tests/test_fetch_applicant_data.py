from datetime import datetime
import logging

from odoo.tests import tagged, common

_logger = logging.getLogger(__name__)


@tagged('ipf')
class TestFetchApplicantData(common.TransactionCase):

    def test_fetch_job_seeker_id(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Job Seeker ID',
            'personal_number': '192001059266'
        })
        self.assertFalse(new_user.job_seeker_id)

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)

        all_applicants.fetch_job_seeker_details()

        self.assertTrue(new_user.job_seeker_id == 1)
        self.assertTrue(new_user.first_name == 'Marie')
        self.assertTrue(new_user.last_name == 'Testare')

        _logger.info("========================================="
                     " IPF TEST fetch_job_seeker_id PASSED "
                     "=========================================")

    def test_fetch_education_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Education Details',
            'personal_number': '192001059266',
            'job_seeker_id': 1
        })
        new_user_higher = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User Higher',
            'last_name': 'Education Details',
            'personal_number': '193001110018',
            'job_seeker_id': 10
        })
        self.assertFalse(new_user.completed_secondary_education)
        self.assertTrue(new_user.education_level == '--')
        self.assertFalse(new_user_higher.completed_secondary_education)
        self.assertTrue(new_user_higher.education_level == '--')

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)
        self.assertIn(new_user_higher, all_applicants)

        all_applicants.fetch_education_details()

        self.assertFalse(new_user.completed_secondary_education)
        self.assertTrue(new_user.education_level == '1')

        self.assertTrue(new_user_higher.completed_secondary_education)
        self.assertTrue(new_user_higher.education_level == '4')

        _logger.info("========================================="
                     " IPF TEST fetch_education_details PASSED "
                     "=========================================")

    def test_fetch_own_business_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Own Business Details',
            'personal_number': '198103314731'
        })
        self.assertTrue(new_user.no_own_business == '--')

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)

        all_applicants.fetch_own_business_details()

        self.assertTrue(new_user.no_own_business == 'examine')

        _logger.info("============================================"
                     " IPF TEST fetch_own_business_details PASSED "
                     "============================================")

    def test_fetch_registered_as_unemployed_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Registered as Unemployed',
            'personal_number': '192001059266'
        })
        self.assertFalse(new_user.registration_date)

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)

        all_applicants.fetch_registered_as_unemployed_details()

        self.assertTrue(new_user.registration_date)
        self.assertTrue(new_user.registration_date < datetime.now().date())

        _logger.info("========================================================"
                     " IPF TEST fetch_registered_as_unemployed_details PASSED "
                     "========================================================")

    def test_fetch_migration_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Migration Details',
            'job_seeker_id': 1
        })
        self.assertFalse(new_user.classification_code_id)
        self.assertFalse(new_user.residence_permit_expires_date)
        self.assertTrue(new_user.residence_permit_type == '--')

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)

        all_applicants.fetch_migration_details()

        self.assertTrue(new_user.classification_code_id.name == 'A7')
        self.assertTrue(new_user.residence_permit_expires_date)
        self.assertTrue(new_user.residence_permit_type == 'put')

        _logger.info("=============================================="
                     " IPF TEST fetch_migration_details PASSED "
                     "==============================================")

    def test_fetch_contact_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User 1',
            'last_name': 'Contact Details',
            'job_seeker_id': 1
        })
        new_user_2 = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User 2',
            'last_name': 'Contact Details',
            'job_seeker_id': 7
        })

        self.assertFalse(new_user.email)
        self.assertFalse(new_user.phone_home)
        self.assertFalse(new_user.phone_mobile)
        self.assertFalse(new_user.phone_work)

        self.assertFalse(new_user_2.email)
        self.assertFalse(new_user_2.phone_home)
        self.assertFalse(new_user_2.phone_mobile)
        self.assertFalse(new_user_2.phone_work)

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)
        self.assertIn(new_user_2, all_applicants)

        all_applicants.fetch_contact_details()

        self.assertTrue(new_user.email == 'mesut.ozil@asyl.nu')
        self.assertTrue(new_user.phone_home == '08-6566656')
        self.assertTrue(new_user.phone_mobile == '070-11223344')
        # Work phone is not populated for job_seeker_id 1 in T1
        self.assertFalse(new_user.phone_work)

        self.assertTrue(new_user_2.email == 'test@test.se')
        self.assertTrue(new_user_2.phone_mobile == '076-1254171')
        self.assertTrue(new_user_2.phone_work == '0-0')
        # Home phone is not populated for job_seeker_id 7 in T1
        self.assertFalse(new_user_2.phone_home)

        _logger.info("============================================"
                     " IPF TEST fetch_contact_details PASSED "
                     "============================================")

    def test_fetch_office_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'AF Office Details',
            'job_seeker_id': 1
        })
        self.assertFalse(new_user.af_office_code)
        self.assertFalse(new_user.af_office_name)
        self.assertFalse(new_user.af_case_worker)

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)

        all_applicants.fetch_office_details()

        self.assertTrue(new_user.af_office_code == '1208')
        self.assertTrue(new_user.af_office_name == 'Af Huvudkontoret')
        self.assertTrue(new_user.af_case_worker == 'bysmr')

        _logger.info("==========================================="
                     " IPF TEST fetch_office_details PASSED "
                     "===========================================")

    def test_fetch_country_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Origin Sweden Details',
            'personal_number': '195003072260'
        })
        new_user_2 = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Origin Australia Details',
            'personal_number': '196512732394'
        })
        new_user_3 = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Origin Norway Details',
            'personal_number': '194005650512'
        })
        self.assertTrue(new_user.origin_country == '--')
        self.assertTrue(new_user_2.origin_country == '--')
        self.assertTrue(new_user_3.origin_country == '--')

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)
        self.assertIn(new_user_2, all_applicants)
        self.assertIn(new_user_3, all_applicants)

        all_applicants.fetch_country_details()

        self.assertTrue(new_user.origin_country == 'sweden')
        self.assertTrue(new_user_2.origin_country == 'non-eu')
        self.assertTrue(new_user_3.origin_country == 'scandinavia')

        _logger.info("============================================"
                     " IPF TEST fetch_country_details PASSED "
                     "============================================")

    def test_fetch_all_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'ALL Details',
            'personal_number': '192001059266'
        })

        all_applicants = self.env['casemanagement.applicant'].search([])
        all_applicants.fetch_all_details()

        self.assertTrue(new_user.job_seeker_id == 1)

        self.assertTrue(new_user.classification_code_id.name == 'A7')
        self.assertTrue(new_user.residence_permit_expires_date)
        self.assertTrue(new_user.residence_permit_type == 'put')

        self.assertTrue(new_user.education_level == '1')

        # TODO: Add checking of own business details
        # TODO: Add checking of origin country details

        self.assertTrue(new_user.registration_date)

        self.assertTrue(new_user.email == 'mesut.ozil@asyl.nu')
        self.assertTrue(new_user.phone_home == '08-6566656')
        self.assertTrue(new_user.phone_mobile == '070-11223344')

        self.assertTrue(new_user.af_office_code == '1208')
        self.assertTrue(new_user.af_office_name == 'Af Huvudkontoret')
        self.assertTrue(new_user.af_case_worker == 'bysmr')

        _logger.info("============================================"
                     " IPF TEST fetch_all_details PASSED "
                     "============================================")
