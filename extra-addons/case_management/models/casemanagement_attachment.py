import base64
import mimetypes

from odoo import models, fields, api, _
from odoo.tools.mimetypes import guess_mimetype


class Attachment(models.Model):
    _name = 'casemanagement.attachment'
    _description = 'File attached to record'
    _order = 'id desc'
    _translate = True

    name = fields.Char(help='File name including extension')
    base64_data = fields.Binary(help='File binary contents')
    mimetype = fields.Char(help='File mimetype')
    download_url = fields.Char(help='Download URL', compute="_compute_download_url")

    # Belongs to Comment
    comment_id = fields.Many2one('casemanagement.comment', 'Comment', readonly=True)

    @api.depends('name')
    def _compute_download_url(self):
        for record in self:
            record.download_url = f'/casemanagement/attachment/download/{record.id}'

    @api.model_create_multi
    def create(self, vals_list):
        for val in vals_list:
            if not val.get('mimetype'):
                val['mimetype'] = self.guess_mimetype(val)

        res_ids = super(Attachment, self).create(vals_list)

        return res_ids

    @staticmethod
    def guess_mimetype(record):
        """
        Guess mimetype based on file name first and on binary data second, if mimetype value not defined
        :param record: Dict with values for Attachment record
        :rtype: str
        """
        mimetype = record.get('mimetype')
        file_name = record.get('name')
        data = record.get('base64_data')

        if not mimetype and file_name and '.' in file_name:
            mimetype = mimetypes.guess_type(file_name)[0]

        if not mimetype and data:
            mimetype = guess_mimetype(base64.b64decode(data))

        return mimetype or 'application/octet-stream'
