# Setup instructions

## Repository and Project Directory

Create `oak` directory that will contain repositories used in this project:

`mkdir oak && cd oak`

Clone this repository to `oak` directory:

`git clone https://gitlab.com/arbetsformedlingen/individdata/oak/cms.git`

Clone repository dealing with issuance of Verifiable Credentials:

`git clone https://gitlab.com/arbetsformedlingen/individdata/oak/solid-client-node-experimentation.git`

## Docker Image

Navigate to `docker` directory inside the `cms` repository:

`cd cms/docker`

Build Docker image based on `odoo:15.0` and tag it with `odoo-jobtech:15.0` (this image is used in `docker-compose.yml` file):

`docker build -t odoo-jobtech:15.0 .`

After successful build, the whole stack can be run with `docker-compose` command (from inside directory containing the `docker-compose.yml` file):

`docker-compose up -d`

If the above step goes well, navigate to [localhost:8069](http://localhost:8069/) and login with `admin:admin`.

## Development

By default, `docker-compose.yml` enables automatic server reloading when Python files are modified. This is done with `--dev xml,reload` flag.

The module `case_management` is installed by default, achieved with the `-i base,case_management` flag.

Tests marked with `casemanagement` are run by default on server restart, achieved with the `--test-tags casemanagement` flag.

(If automatic reloading does not work) After changing code, restart `odoo` container either in Docker GUI or on command line:

`docker stop $(docker ps -f name=odoo --quiet) && docker start odoo`

Go to [localhost:8069](http://localhost:8069/) and login with `admin:admin`.

Go to [localhost:8069/casemanagement](http://localhost:8069/casemanagement) to see the default module page.

Optional: if you want random data added, run `python3 util/generate-example-data.py` (Verify the database connection settings)

## Manual Module (Re)Install

Find `case_management` module on the module list page (remove the default App filter to see modules).

Click the install button and wait until the first time setup is finished (alternatively, check the logs for `odoo` container to see error messages).

**Congratulations!** Now the local development environment should be up and running.
