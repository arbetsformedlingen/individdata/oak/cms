import json
import logging

from odoo.tests import tagged, common

_logger = logging.getLogger(__name__)


@tagged('ipf')
class TestFetchCaseWorkerData(common.TransactionCase):

    def test_fetch_case_worker_data(self):
        with open('/mnt/extra-addons/fetch_case_worker_data/tests/x500-persons-T2-response.json', 'r') as f:
            json_data = json.loads(f.read())

        for d in json_data:
            self.assertTrue('officeCode' in d, f'officeCode not found in "{d}"')

        all_users = self.env['res.users'].search([])
        transformed_data = all_users._transform_data(json_data)

        self.assertIsInstance(transformed_data, dict)
        for k, v in transformed_data.items():
            self.assertTrue(len(k) == 5, f'Login length not 5 characters: "{k}"')
            self.assertIsInstance(v, dict)

        test_login = 'xxakg'
        self.assertIn(test_login, transformed_data)

        new_user = self.env['res.users'].create({
            'name': 'Test User',
            'login': test_login
        })
        self.assertFalse(new_user.office_code)

        all_users = self.env['res.users'].search([])
        self.assertIn(new_user, all_users)

        all_users.fetch_case_worker_data()

        self.assertTrue(new_user.office_code == '9194')

        _logger.info("========================================="
                     " IPF TEST fetch_case_worker_data PASSED "
                     "=========================================")
