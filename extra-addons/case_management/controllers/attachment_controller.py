# -*- coding: utf-8 -*-
import base64
import logging

from werkzeug import Response

from odoo import http
from odoo.http import request, content_disposition

from ..util.serialize_exception import serialize_exception
from ..util.email import send_email_with_attachment, extract_attachment_data

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)


class AttachmentController(http.Controller, CaseManagementHelper):
    @http.route('/casemanagement/attachment/download/<int:attachment_id>',
                methods=['GET'],
                auth='user')
    @serialize_exception
    def download_attachment(self, attachment_id):
        try:
            fetched_attachment = http.request.env['casemanagement.attachment'].browse([attachment_id])
            if fetched_attachment:
                binary_data = base64.b64decode(fetched_attachment.base64_data)
                response = request.make_response(
                    binary_data,
                    headers=[('Content-Type', fetched_attachment.mimetype),
                             ('Content-Disposition', content_disposition(fetched_attachment.name)),
                             ('Content-Length', len(binary_data))],
                )
            else:
                response = Response(status=404)
        except Exception as e:
            _logger.error(f"Problem when downloading attachment with ID '{attachment_id}': {e}")
            response = Response(status=404)

        return response

    @http.route('/casemanagement/<model_name>/attach/',
                methods=['POST', 'GET'],
                auth='user',
                website=True)
    @serialize_exception
    def attach_file_to_object(self, model_name, **post):
        """
        Attach file to a given Model (Application/Applicant/Employer).
        Expected fields are 'attachment' and 'application_id/applicant_id/employer_id'
        :param model_name: One of application/applicant/employer
        :type model_name: str
        """
        assert model_name in ['application', 'applicant', 'employer']

        object_id = int(post.get(f'{model_name}_id', 0))
        objects = http.request.env[f'casemanagement.{model_name}'].search([])
        if not object_id:
            return http.request.render('case_management.attach-file',
                                       {'objects': objects,
                                        'model_name': model_name,
                                        'modal_message': self.pop_flash_from_session()})

        try:
            fetched_object = http.request.env[f'casemanagement.{model_name}'].browse([object_id])
            if not fetched_object:
                response_json = dict(status="error", error=f"No {model_name} with ID '{object_id}'")
            else:
                attachment_data = extract_attachment_data(post)
                http.request.env["attachment.helper"].attach_document(fetched_object,
                                                                      attachment_data.get('name'),
                                                                      attachment_data.get('binary'),
                                                                      attachment_data.get('comment_text'))

                return http.request.render('case_management.attach-file',
                                           {'objects': objects,
                                            'model_name': model_name,
                                            'modal_message': 'File attachment saved'})
        except Exception as e:
            _logger.error(f"Error when attaching to {model_name} ID '{object_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route('/casemanagement/<model_name>/email/',
                methods=['POST', 'GET'],
                auth='user',
                website=True)
    @serialize_exception
    def email_with_attachment_example(self, model_name, **post):
        """
        Send email to a given Applicant/Employer.
        Expected fields are 'subject', 'content', 'attachment', 'applicant_id'/'employer_id'
        :param model_name: Model name, one of applicant/employer
        :type model_name: str
        """
        assert model_name in ['applicant', 'employer']

        object_id = int(post.get(f'{model_name}_id', 0))
        objects = http.request.env[f'casemanagement.{model_name}'].search([['email', '!=', None]])
        if not object_id:
            return http.request.render('case_management.send-email',
                                       {'objects': objects,
                                        'model_name': model_name,
                                        'modal_message': self.pop_flash_from_session()})

        try:
            fetched_object = http.request.env[f'casemanagement.{model_name}'].browse([object_id])
            if not fetched_object:
                response_json = dict(status="error", error=f"No {model_name} with ID '{object_id}'")
            else:
                # Check if there is an attachment, send email
                send_email_with_attachment(fetched_object, post, http.request.env)
                return http.request.render('case_management.send-email',
                                           {'objects': objects,
                                            'model_name': model_name,
                                            'modal_message': 'Email sent'})
        except Exception as e:
            _logger.error(f"Error when emailing {model_name} ID '{object_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")
