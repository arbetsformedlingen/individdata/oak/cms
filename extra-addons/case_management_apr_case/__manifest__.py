# -*- coding: utf-8 -*-
{
    'name': "Casemanagement APR case",
    'summary': "APR cases",
    'description': "",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',
    'depends': ['case_management', "res_ssyk"],
    'data': [
        'security/ir.model.access.csv'
    ],
}
