import logging
import os

from odoo import models, fields

_logger = logging.getLogger(__name__)

POD_PROVIDER_BASE_URL = os.environ.get("POD_PROVIDER_BASE_URL", "http://localhost:3000")


class WebhookSubscription(models.Model):
    _name = 'casemanagement.subscription'
    _description = 'Solid webhook subscription'
    _order = "id desc"
    _translate = True

    _sql_constraints = [
        ('digest_uniq', 'UNIQUE (digest)',
         'You can not have two Subscriptions with the same digest!')
    ]

    digest = fields.Char(
        help='Digest of JSON document creating subscription',
        readonly=True)

    type = fields.Char(
        help='Type as string',
        readonly=True)

    subscription_target = fields.Char(
        help='URI of object in Solid we get notified about',
        readonly=True)

    unsubscribe_endpoint = fields.Char(
        help='URL used to unsubscribe from further notifications',
        readonly=True)

    raw_json = fields.Char(
        help='JSON string payload received when subscription from Solid confirmed',
        readonly=True)

    def make_sure_subscribed(self):
        """
        Checks if a subscription to Solid Pod server inbox exists and creates it if not
        """
        subscription_target = f'{POD_PROVIDER_BASE_URL}arbetsformedlingen/egendata/inbox/'
        subscription = self.env['casemanagement.subscription'].search([
            ['subscription_target', 'like', subscription_target]
        ]).read()

        if subscription:
            _logger.info(f"Subscription to '{subscription_target}' exists")
        else:
            _logger.info(f"Subscription to '{subscription_target}' does not exist")
            session = dict()
            from ..util.solid import subscribe_resource
            subscribe_resource(self.env, subscription_target, session)
