# -*- coding: utf-8 -*-
{
    'name': "Fetch Applicant Data",
    'summary': "fetch data for applicant",
    'description': "",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',
    'depends': [
        'case_management',
        'af_ipf'
    ],
    'data': [],
}
