from odoo import models, fields


class Import(models.Model):
    _name = 'casemanagement.import'
    _description = 'CSV import of Employers'
    _order = 'id desc'
    _translate = True

    file_name = fields.Char(help='File name including extension')
    csv_file = fields.Binary(help='CSV import file binary contents (base64-encoded string)')
    import_result = fields.Text(help='Result of import as text')

    # Belongs to Campaign
    campaign_id = fields.Many2one('casemanagement.campaign', 'Campaign', readonly=True)
