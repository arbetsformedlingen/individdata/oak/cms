# -*- coding: utf-8 -*-
{
    'name': "IPF Daganteckning POST",
    'summary': "POST Daganteckning data to TRASK from CMS",
    'description': "",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',
    'depends': [
        'case_management',
        'af_ipf'
        ],
    'data': [
        'security/ir.model.access.csv',
    ],
}
