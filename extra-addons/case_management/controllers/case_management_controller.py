# -*- coding: utf-8 -*-
import logging
import os

from werkzeug import Response

from odoo.tools import date_utils
from ..util.api import migration_details_by_seeker_id
from ..util.serialize_exception import serialize_exception

from odoo import http

import math
import json

from ..util.vc import NodeVC

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)

# Constants
SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3000")
SECRET_SOLID_SERVER_EMAIL = os.environ.get("SECRET_SOLID_SERVER_EMAIL", "source@example.com")
SECRET_SOLID_SERVER_PW = os.environ.get("SECRET_SOLID_SERVER_PW", "source")
NODE_VC_SCRIPT_PATH = os.environ.get("NODE_VC_SCRIPT_PATH", '/var/oak/solid-client-node-experimentation/issue-vc.js')


class CaseManagement(http.Controller, CaseManagementHelper):

    @http.route('/casemanagement', auth='user', website=True)
    def index(self, page=1, applications_per_page=25, **kw):
        # TODO: modal_message passed via session
        current_page = page  # Grab from query param

        applications_per_page_int = int(applications_per_page)

        db_offset = (int(page) - 1) * int(applications_per_page)

        applications = http.request.env["casemanagement.application"] \
            .search([['approval', 'not in', ['approved', 'rejected']]],
                    limit=applications_per_page_int,
                    offset=db_offset)

        total_applications = http.request.env["casemanagement.application"] \
            .search([['approval', 'not in', ['approved', 'rejected']]], count=True)

        pages = math.ceil(total_applications / applications_per_page_int)

        return http.request.render("case_management.casemanagementgrid", {
            "root": "/casemanagement",
            "applications": applications,
            "tot_pages": pages,
            "current_page": int(current_page),
            "modal_message": self.pop_flash_from_session(),
        })

    @http.route("/casemanagement/migration/<int:job_seeker_id>",
                methods=["GET"],
                auth="user",
                website=True)
    def get_migration_details(self, job_seeker_id):
        """
        Get information from API endpoint
        :param job_seeker_id: AFs internal job seeker ID
        :type job_seeker_id: int
        """
        result = json.dumps(migration_details_by_seeker_id(job_seeker_id), indent=4)
        _logger.info("Migration Details:")
        _logger.info(result)

        return http.request.render('case_management.get-migration-details', {"result": result})

    @http.route("/casemanagement/<model_name>/<int:record_id>",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_record_by_id(self, model_name, record_id):
        """
        Get Employer by ID
        :param record_id: Employer ID
        :type record_id: int
        :param model_name: Model name in casemanagement module (employer/applicant/application)
        :type model_name: str
        """
        assert model_name in ['applicant', 'application', 'employer']
        try:
            fetched_employer = http.request.env[f'casemanagement.{model_name}'].browse(
                [record_id]
            ).read()

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_employer, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error(f"Error '{e}' when fetching record '{model_name}' by ID '{record_id}'")
            response = Response("[]", content_type="application/json")

        return response

    @http.route("/casemanagement/<model_name>/",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_all_records(self, model_name):
        """
        Get all records of a given model
        :param model_name: Model name in casemanagement module (employer/applicant/application)
        :type model_name: str
        """
        assert model_name in ['applicant', 'application', 'employer']
        try:
            domain = []
            if model_name == 'application':
                # filter out processed Application records
                domain.append(['approval', 'not in', ['approved', 'rejected']])

            fetched_records = http.request.env[f'casemanagement.{model_name}'].search(
                domain
            ).read()

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_records, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error(f"Error '{e}' when fetching all records from model '{model_name}'")
            response = Response("[]", content_type="application/json")

        return response

    @http.route('/casemanagement/vc/',
                methods=['POST', 'GET'],
                auth='user',
                website=True)
    @serialize_exception
    def issue_vc(self, **post):
        issuer = NodeVC(NODE_VC_SCRIPT_PATH)
        result = issuer.issue_vc('890909-0000', '', **post)

        return Response(json.dumps(result), content_type="application/json")

    @http.route("/casemanagement/whoami",
                methods=["PUT"],
                auth="public",
                type="json")
    def whoami(self):
        return http.request.env.user.read()
