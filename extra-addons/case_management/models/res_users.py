from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class ResUsers(models.Model):
    _inherit = 'res.users'

    office_code = fields.Char(string="Office code", help="AF office code", readonly=True)

    @api.constrains('office_code')
    def _constrain_office_code(self):
        for rec in self:
            if rec.office_code:
                if not self.env['casemanagement.validation'].valid_office_code(rec.office_code):
                    raise ValidationError(_("Invalid Office code"))
