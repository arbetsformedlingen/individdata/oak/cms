import logging

from personnummer.personnummer import Personnummer

from odoo.exceptions import ValidationError
from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)


class Applicant(models.Model):
    _name = 'casemanagement.applicant'
    _inherit = ['casemanagement.person', 'mail.thread']
    _description = 'Applicant applying for an internship'
    _order = "id desc"
    _sql_constraints = [
        ('personal_num_uniq', 'UNIQUE (personal_number)',
         'You can not have two applicants with the same personal number!')
    ]
    _translate = True

    personal_number = fields.Char(
        help='Swedish personal number',
        readonly=True)

    personal_number_ten_digits = fields.Char(
        compute='_compute_ten_digits',
        help='Swedish personal number as 10 digits',
        readonly=True)

    personal_number_twelve_digits = fields.Char(
        compute='_compute_twelve_digits',
        help='Swedish personal number as 12 digits',
        readonly=True)

    century = fields.Char(
        compute='_compute_century',
        help='Century in Swedish personal number',
        readonly=True)

    job_seeker_id = fields.Integer(
        help='AF job seeker ID',
        readonly=True)

    name = fields.Char(compute="_render_name",
                       help='Applicant full name',
                       readonly=True,
                       store=True)

    email = fields.Char(
        help='Email address'
    )

    address_id = fields.Many2one("casemanagement.address", "Address")

    registration_date = fields.Date(
        help='Date registered with AF as looking for job',
        readonly=True)

    residence_permit_expires_date = fields.Date(
        help='Date when the latest residence permit expires',
        readonly=True)

    classification_code_id = fields.Many2one('casemanagement.code', 'Classification Code')

    residence_permit_type = fields.Selection(
        selection=[
            ('put', 'Permanent Residence Permit'),
            ('b-put', 'Permanent Residence Permit'),
            ('uat', 'Residence and Work Permit'),
            ('uat-g', 'Residence and Work Permit'),
            ('t-uat', 'Temporary Residence and Work Permit'),
            ('t-ut', 'Temporary Residence Permit'),
            ('ut', 'Residence Permit'),
            ('uk', 'Residence Card'),
            ('--', '--'),
        ],
        default='--',
        help='PUT/T-UAT/T-UT/UT/UK - residence permit type',
        readonly=True)
    residence_permit_type_good = fields.Boolean(
        compute='_suggest_residence_type_good',
        help='Residence permit eligible',
        readonly=True
    )

    origin_country = fields.Selection(
        selection=[
            ('sweden', 'Sweden'),
            ('scandinavia', 'Scandinavia'),
            ('non-eu', 'non-EU'),
            ('eu', 'EU'),
            ('--', '--'),
        ],
        default='--',
        help='Sweden/Scandinavia/non-EU/EU/-- - country of origin')

    auto_suggestion_approval = fields.Selection(
        compute='_suggest_approval',
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
        ],
        help='Eligible/Examine - automatically generated value based on Applicant properties',
        default='examine',
        readonly=True)

    approval = fields.Selection(
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
            ('--', '--'),
        ],
        default='--',
        help='Eligible/Examine/-- - decided by case worker')

    solid_guid = fields.Char(
        help='GUID used to identify Solid resources belonging to Applicant',
        readonly=True)

    application_ids = fields.One2many('casemanagement.application', 'applicant_id', string='Applications')

    comment_ids = fields.One2many('casemanagement.comment', 'applicant_id', string='Comments')

    completed_secondary_education = fields.Boolean(compute='_compute_secondary_education',
                                                   readonly=True,
                                                   string="Completed secondary education", default=False)

    # TODO: Translate properly
    education_level = fields.Selection(
        selection=[
            ('0', '0 Saknar formell grundläggande utbildning'),
            ('1', '1 Förgymnasial utbildning kortare än 9 år'),
            ('2', '2 Förgymnasial utbildning 9 (10) år'),
            ('3', '3 Gymnasial utbildning'),
            ('4', '4 Eftergymnasial utbildning, kortare än två år'),
            ('5', '5 Eftergymnasial utbildning, två år eller längre'),
            ('6', '6 Forskarutbildning'),
            ('--', '--'),
        ],
        default='--',
        help='0/1/2/3/4/5/6/-- from API',
        readonly=True)

    samordning_av_ersattning = fields.Boolean(string="Samordning av ersättning", default=True)

    no_own_business = fields.Selection(
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
            ('--', '--'),
        ],
        default='--',
        help='Eligible/Examine/-- - based on API response',
        readonly=True)

    af_office_code = fields.Char(
        help='AF office code where Applicant was registered',
        readonly=True)

    af_office_name = fields.Char(
        help='AF office name where Applicant was registered',
        readonly=True)

    af_case_worker = fields.Char(
        help='Responsible AF case worker',
        readonly=True)

    @api.depends('first_name', 'last_name')
    def _render_name(self):
        """
        Renders Applicant full name
        """
        for record in self:
            record.name = f"{record.first_name} {record.last_name}"

    @api.depends('name', 'email', 'personal_number', 'classification_code_id', 'residence_permit_type_good',
                 'origin_country', 'registration_date', 'phone_home', 'phone_mobile', 'phone_work')
    def _suggest_approval(self):
        """
        Computes automatic approval suggestion based on property values supplied in @api.depends decorator
        """
        for record in self:
            phone_number_exists = any([record.phone_home, record.phone_mobile, record.phone_work])
            satisfied = all(
                [record.name, record.email, record.personal_number, record.residence_permit_type_good,
                 record.registration_date, phone_number_exists, record.origin_country == 'non-eu'])

            if record.classification_code_id:
                code_eligible = record.classification_code_id.eligible == 'true'
            else:
                code_eligible = False

            record.auto_suggestion_approval = 'eligible' if satisfied and code_eligible else 'examine'

    @api.depends('residence_permit_type')
    def _suggest_residence_type_good(self):
        """
        Determines if residence_permit_type is approved
        """
        for record in self:
            # TODO: discuss ambiguous types like b-put, uat-g
            record.residence_permit_type_good = record.residence_permit_type in ['put', 'b-put', 'uat',
                                                                                 'uat-g', 't-uat']

    @api.depends('education_level')
    def _compute_secondary_education(self):
        """
        Computes if Applicant completed secondary education
        """
        for record in self:
            if record.education_level and record.education_level != '--':
                record.completed_secondary_education = int(record.education_level) > 2
            else:
                record.completed_secondary_education = False

    @api.depends('personal_number')
    def _compute_century(self):
        for record in self:
            if record.personal_number:
                personal_number_parts = Personnummer.get_parts(record.personal_number)
                record.century = personal_number_parts.get('century')
            else:
                record.century = False

    @api.depends('personal_number')
    def _compute_ten_digits(self):
        for record in self:
            if record.personal_number:
                ten_digits = Personnummer(record.personal_number).format(long_format=True)[2:]
                record.personal_number_ten_digits = ten_digits
            else:
                record.personal_number_ten_digits = False

    @api.depends('personal_number')
    def _compute_twelve_digits(self):
        for record in self:
            if record.personal_number:
                twelve_digits = Personnummer(record.personal_number).format(long_format=True)
                record.personal_number_twelve_digits = twelve_digits
            else:
                record.personal_number_twelve_digits = False

    @api.constrains('email')
    def _check_email(self):
        for record in self:
            if not self.env['casemanagement.validation'].valid_email(record.email):
                raise ValidationError("Field 'email' has to be a valid email address")

    @api.constrains('personal_number')
    def _check_personal_number(self):
        for record in self:
            if not self.env['casemanagement.validation'].valid_personal_number(record.personal_number):
                raise ValidationError("Field 'personal_number' has to be a valid personal number")

    def write(self, values):
        to_return = list()
        for rec in self:
            old_values = {}
            for key in values:
                old_values[key] = getattr(rec, key)
            res = super(Applicant, rec).write(values)
            to_return.append(res)
            body = []
            for key in values:
                field_name = self.env['ir.translation'].get_field_string(rec._name)[key]
                body.append(_("'%(field_name)s' field was changed from '%(old_value)s' to '%(new_value)s'",
                              field_name=field_name,
                              old_value=old_values[key],
                              new_value=values[key]))
            body = "\n".join(body)
            rec.message_post(body=_(body))
            _logger.info(f"{body} for {rec._name} {rec.id}")
        return to_return
