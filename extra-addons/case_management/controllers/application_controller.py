# -*- coding: utf-8 -*-
import logging
import traceback

from werkzeug import Response

from odoo.tools import date_utils

from odoo import http
from odoo.http import request

import json

from ..util.serialize_exception import serialize_exception
from ..util.solid import put_response_to_solid

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)


class Application(http.Controller, CaseManagementHelper):
    @http.route('/casemanagement/application/approve/<int:application_id>',
                methods=['GET'],
                auth='user')
    @serialize_exception
    def application_approve_get(self, application_id):
        """
        Approve Application with given application_id
        :param application_id: Application ID to approve
        :type application_id: int
        """
        try:
            fetched_application = http.request.env['casemanagement.application'].browse([application_id])
            fetched_application.mark_approved()

            put_response_to_solid(http.request.env,
                                  fetched_application.applicant_id.personal_number,
                                  fetched_application.applicant_id.solid_guid)

            self.set_flash_in_session(
                f"Application approved and VC issued for '{fetched_application.applicant_id.name}'!")

        except Exception as e:
            error_message = f"Error when approving Application by ID '{application_id}': '{e}'"
            _logger.error(error_message)
            self.set_flash_in_session(error_message)

        return request.redirect('/casemanagement')

    @http.route('/casemanagement/application/reject/<int:application_id>',
                methods=['POST'],
                auth='user',
                type='json')
    @serialize_exception
    def application_reject(self, application_id):
        """
        Reject Application with given application_id
        :param application_id: Application ID to reject
        :type application_id: int
        """
        try:
            fetched_application = http.request.env['casemanagement.application'].browse([application_id])
            fetched_application.mark_rejected()

            response = self._response_from_update(application_id, fetched_application.read())
        except Exception as e:
            _logger.error(f"Error when rejecting Application by ID '{application_id}': '{e}'")
            response = Response("{}", content_type="application/json")

        return response

    @staticmethod
    def _response_from_update(application_id, updated_application):
        """
        Produces a JSON response from updated Application object
        :param application_id: Application ID, int
        :param updated_application: Updated Application object
        :rtype: Response
        """
        # Datetime values can not be serialized out of the box, so odoo provides a serializer
        json_result = json.dumps(updated_application, indent=4, default=date_utils.json_default)
        if not updated_application:
            _logger.error(f"No Application found by ID '{application_id}'")

        return Response(json_result, content_type="application/json")

    @staticmethod
    def get_applications_by_applicant(applicant_id):
        applications = http.request.env["casemanagement.application"].search([
            ("applicant_id", "=", applicant_id)
        ]).read()

        return applications

    @http.route("/casemanagement/application/<int:application_id>/approve",
                methods=["PUT"],
                auth="user",
                type="json")
    def approve_application(self, application_id):
        json_data = request.jsonrequest

        response = self._not_key_and_application_exists("note", application_id, json_data)
        if not response:
            try:
                application = http.request.env['casemanagement.application'].browse([application_id])
                application.write({"approval": "eligible"})
                http.request.env["attachment.helper"].attach_document(application, None, None,
                                                                      "Ändrade behörighetsbeslut till 'Behörig' \n"
                                                                      "Anteckning: '%s'" % json_data["note"])
                return application.read()
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @staticmethod
    def _not_key_and_application_exists(key, application_id, json_data):
        """
        Returns Response with error status code if key or application_id doesn't exist otherwise False.
        """
        if key not in json_data:
            return Response("", status=400)

        application = http.request.env["casemanagement.application"].browse(application_id)

        if not application:
            return Response("", content_type="application/json", status=404)

        return False

    @http.route("/casemanagement/application/<int:application_id>/set_approval",
                methods=["PUT"],
                auth="user",
                type="json")
    def set_application_approval(self, application_id):
        json_data = request.jsonrequest

        response = self._not_key_and_application_exists("approval", application_id, json_data)
        if not response:
            try:
                application = http.request.env['casemanagement.application'].browse([application_id])
                application.write({"approval": json_data["approval"]})
                http.request.env["attachment.helper"].attach_document(application, None, None,
                                                                      "Ändrade behörighetsbeslut till '%s'" % json_data[
                                                                          "approval"])
                return Response(status=200, content_type="appication/json")
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @http.route("/casemanagement/application/",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_all_applications(self):

        # TODO: Paginate
        try:

            applications = http.request.env["casemanagement.application"].search([]).read()

            for application in applications:
                applicant_id = application["applicant_id"][0]
                applicant = http.request.env["casemanagement.applicant"].search([
                    ("id", "=", applicant_id)]
                ).read()[0]

                application["applicant"] = applicant

                # Include comments made on Applicant
                activity_log_entries = http.request.env["casemanagement.comment"].search([
                    ("applicant_id", "=", applicant["id"])]
                ).read()

                # Append comments made on Application itself
                activity_log_entries.extend(
                    http.request.env["casemanagement.comment"].search([
                        ("application_id", "=", application["id"])]
                    ).read()
                )

                application["activity_log"] = activity_log_entries

                employer_id = application["employer_id"][0]
                employer = http.request.env["casemanagement.employer"].search([
                    ("id", "=", employer_id)]
                ).read()[0]

                application["employer"] = employer

            json_result = json.dumps(applications, indent=4, default=date_utils.json_default)

            response = Response(json_result, content_type="application/json")

        except Exception as e:
            traceback.print_exc()
            _logger.error("Error '%s' when fetching applications" % e)
            response = Response("[]", content_type="application/json")

        return response

    @http.route('/casemanagement/application/approve/<int:applicant_id>',
                methods=['POST'],
                auth='user',
                type='json')
    @serialize_exception
    def application_approve(self, applicant_id):
        """
        Approve Application with given applicant_id
        :param applicant_id: Applicant ID of Application to approve
        :type applicant_id: int
        """
        try:
            fetched_applicants = http.request.env['casemanagement.applicant'].browse(applicant_id)

            if len(fetched_applicants) < 1:
                response = Response("Applicant not found", status=404, content_type="application/json")
                return response

            fetched_applicant = fetched_applicants[0]
            print(fetched_applicant)

            application_ids = fetched_applicant.application_ids

            if len(application_ids) < 1:
                response = Response("Application not found", status=404, content_type="application/json")
                return response

            fetched_application = http.request.env['casemanagement.application'].browse([application_ids])
            updated_application = fetched_application.mark_approved().read()

            response = self._response_from_update(updated_application.id, updated_application)

            put_response_to_solid(http.request.env,
                                  fetched_application.applicant_id.personal_number,
                                  fetched_application.applicant_id.solid_guid)

        except Exception as e:
            _logger.error("Error when approving Application by ID '%s': '%s'" % (applicant_id, e))
            response = Response("{}", content_type="application/json")

        return response

    @http.route('/casemanagement/application/reject/<int:application_id>',
                methods=['POST'],
                auth='user',
                type='json')
    @serialize_exception
    def application_reject(self, application_id):
        """
        Reject Application with given application_id
        :param application_id: Application ID to reject
        :type application_id: int
        """
        try:
            fetched_application = http.request.env['casemanagement.application'].browse([application_id])
            updated_application = fetched_application.mark_rejected().read()

            response = self._response_from_update(application_id, updated_application)
        except Exception as e:
            _logger.error("Error when rejecting Application by ID '%s': '%s'" % (application_id, e))
            response = Response("{}", content_type="application/json")

        return response
