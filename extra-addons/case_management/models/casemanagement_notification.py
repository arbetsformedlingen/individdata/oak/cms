import json
import logging
from datetime import datetime

from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class Notification(models.Model):
    _name = 'casemanagement.notification'
    _description = 'Solid webhook notification update'
    _order = "id desc"
    _translate = True

    urn_uuid = fields.Char(
        help='ID property of JSON document',
        readonly=True)

    type = fields.Char(
        help='Type as string',
        readonly=True)

    object = fields.Char(
        help='Object as string',
        readonly=True)

    subscription_target = fields.Char(
        help='URI of object in Solid we get notified about',
        readonly=True)

    unsubscribe_endpoint = fields.Char(
        help='URL used to unsubscribe from further notifications',
        readonly=True)

    raw_json = fields.Char(
        help='JSON string payload received when notification from Solid arrived',
        readonly=True)

    published = fields.Datetime(
        help='Value of published property from Solid notification or current timestamp',
        readonly=True)

    @api.model_create_multi
    def create_from_notification_json(self, input_json):
        """
        Creates records from JSON received as Solid notification webhook
        :param input_json: Dict or list of dicts
        :type input_json: dict | list[dict]
        """
        if type(input_json) == dict:
            result = self.env['casemanagement.notification'].create(
                self._correct_notification_json(input_json)
            )
        elif type(input_json) == list:
            items = [self._correct_notification_json(i) for i in input_json]
            result = self.env['casemanagement.notification'].create(
                items
            )
        else:
            raise ValueError('Incorrect input_json type (expected dict|list): %s' % type(input_json))

        return result

    @staticmethod
    def _correct_notification_json(json_data):
        """
        Transforms notification received from Solid notification webhook into dict that can be sent to create()
        :param json_data: Dict to process
        :type json_data: dict
        """
        try:
            published_datetime_str = json_data.get('published')
            try:
                if published_datetime_str:
                    published_datetime = datetime.strptime(published_datetime_str, '%Y-%m-%dT%H:%M:%S.%fZ')
                else:
                    published_datetime = datetime.now()
            except Exception as e:
                _logger.error("Error '%s' when parsing value '%s' to datetime" % (e, published_datetime_str))
                published_datetime = datetime.now()

            return {
                'urn_uuid': json_data.get('id'),
                'type': json_data.get('type'),
                'object': json_data.get('object'),
                'subscription_target': json_data.get('object', dict()).get('id'),
                'unsubscribe_endpoint': json_data.get('unsubscribe_endpoint'),
                'raw_json': json.dumps(json_data),
                'published': published_datetime
            }
        except Exception as e:
            _logger.error(e)
            return {}
