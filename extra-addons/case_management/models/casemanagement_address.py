from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class CaseManagementAddress(models.Model):
    _name = 'casemanagement.address'
    _description = 'Simple representation of address'

    street = fields.Char(string="Street")
    city = fields.Char(string="City")
    address_zip = fields.Char(string="Zip")

    @api.constrains("address_zip")
    def _constrain_zip(self):
        for rec in self:
            if not self.env['casemanagement.validation'].valid_zip(rec.address_zip):
                raise ValidationError(_("Invalid zip code"))

    @api.constrains("street")
    def _constrain_street(self):
        for rec in self:
            if len(rec.street) > 35:
                raise ValidationError(_("Street name is too long"))

    @api.constrains("city")
    def _constrain_city(self):
        for rec in self:
            if len(rec.city) > 20:
                raise ValidationError(_("City name is too long"))
