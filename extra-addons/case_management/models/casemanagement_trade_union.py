from odoo.exceptions import ValidationError
from odoo import models, fields, api


class TradeUnion(models.Model):
    _name = 'casemanagement.union'
    _description = 'Trade union'
    _order = 'id desc'
    _translate = True

    name = fields.Char(help='Trade union name', required=True)
    email = fields.Char(help='Email address of a representative', required=True)

    approval_ids = fields.One2many('casemanagement.approval', 'union_id', string='Approvals')

    @api.constrains('email')
    def _check_email(self):
        for record in self:
            if not self.env['casemanagement.validation'].valid_email(record.email):
                raise ValidationError("Field 'email' has to be a valid email address")

    @api.model
    def update_email(self, **post):
        """
        Updates email addresses for Union records
        :param post: Data to update email addresses from
        :rtype: list[TradeUnion]
        """
        updated_unions = list()
        for k, v in post.items():
            union_id = int(k.replace('email_', ''))
            union = self.env[self._name].browse([union_id])
            if self.env['casemanagement.validation'].valid_email(v) and union.email != v:
                union.update({'email': v})
                updated_unions.append(union)

        return updated_unions
