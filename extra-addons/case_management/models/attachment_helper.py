import base64

from odoo import models


class AttachmentHelper(models.Model):
    _name = "attachment.helper"
    _description = "Helper base class for handling attachments"

    def attach_document(self, records, file_name, binary_data, comment_text, *args):
        """
        Attach document with comment to given object
        :param records: Record set with record(s) to attach document/comment to
        :type records: recordset
        :param file_name: Attachment name
        :type file_name: str
        :param binary_data: Document binary data
        :type binary_data: bytearray
        :param comment_text: Comment text
        :type comment_text: str
        """
        assert any([file_name, comment_text]), 'Either file_name or comment_text has to be filled'

        for record in records:
            values = {
                'name': file_name,
                'base64_data': base64.b64encode(binary_data or b'')
            }

            attachment = self.env['casemanagement.attachment'].create(values)
            created_comment = self.env['casemanagement.comment'].create({'comment_text': comment_text,
                                                                         'attachment_ids': [(4, attachment.id)]})
            record.update({
                'comment_ids': [(4, created_comment.id)]
            })
        return records
