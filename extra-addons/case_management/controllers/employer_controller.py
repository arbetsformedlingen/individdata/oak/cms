# -*- coding: utf-8 -*-
import logging

from werkzeug import Response

from odoo.tools import date_utils

from odoo import http

import json

from ..util.serialize_exception import serialize_exception
from ..util.email import send_email, extract_attachment_data, JS_EMAIL

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)


class Employer(http.Controller, CaseManagementHelper):
    @http.route('/casemanagement/employer/<action>/<int:employer_id>',
                methods=['GET', 'POST'],
                auth='user',
                website=True)
    @serialize_exception
    def approve_employer(self, action, employer_id, **post):
        """
        Approve/Examine a given Employer, save action as comment, email Employer and JobbSprånget
        :param action: 'approve' or 'examine'
        :type action: str
        :param employer_id: Employer ID to approve/examine
        :type employer_id: int
        """
        assert action in ['approve', 'examine']
        try:
            fetched_employer = http.request.env['casemanagement.employer'].search([['id', '=', employer_id]])
            modal_message = ''
            if fetched_employer:
                fetched_employer = fetched_employer[0]
                if post:
                    bar_number = post.get('bar_number')
                    if bar_number:
                        # TODO: possible to validate bar_number with some API?
                        # Save action/attachment as comment
                        attachment_data = extract_attachment_data(post)
                        http.request.env["attachment.helper"].attach_document(fetched_employer,
                                                                              attachment_data.get('name'),
                                                                              attachment_data.get('binary'),
                                                                              attachment_data.get('comment_text'))
                        # Approve/Examine Employer
                        approval_state = 'approved' if action == 'approve' else 'examine'
                        fetched_employer.update({'approval': approval_state, 'bar_number': bar_number})

                        modal_message = f'Employer "{fetched_employer.name}" marked "{approval_state}"!'

                        # Email Employer
                        if approval_state == 'approved':
                            email_content = f'Congratulations, {fetched_employer.name}!\n\n' \
                                            f'You have been approved as en employer for internships given via ' \
                                            f'JobbSprånget.\n\n' \
                                            f'This is only for your information and does not require any action.'
                        else:
                            email_content = attachment_data.get('comment_text') or 'no message present'

                        email_subject = 'Employer Approved' if approval_state == 'approved' else 'Employer - Examine'

                        # TODO: email the JobbSprånget team - is it OK to just send a CC here?
                        email_sent = send_email(fetched_employer, email_content, http.request.env, email_subject,
                                                cc=JS_EMAIL)
                        if email_sent:
                            modal_message += ' Emails have been sent to Employer and JobbSprånget.'
                        else:
                            modal_message += ' Emails could not be sent!'
                    else:
                        modal_message = f'Employer "{fetched_employer.name}" cannot be approved without BÄR number!'

            return http.request.render('case_management.approve-employer',
                                       {'employer': fetched_employer,
                                        'modal_message': modal_message,
                                        'employer_id': employer_id,
                                        'action': action})
        except Exception as e:
            _logger.error(f"Error when emailing Employer ID '{employer_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route("/casemanagement/employer/<int:employer_id>",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_employer_by_id(self, employer_id):
        """
        Get Employer by ID
        :param employer_id: Employer ID
        :type employer_id: int
        """
        try:
            fetched_employer = http.request.env['casemanagement.employer'].browse(
                [employer_id]
            ).read()

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_employer, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error("Error '%s' when fetching Employer by ID %s" % (e, employer_id))
            response = Response("[]", content_type="application/json")

        return response

    @http.route("/casemanagement/employer/",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_all_employers(self):
        """
        Get all Employers
        """
        try:
            fetched_employers = http.request.env['casemanagement.employer'].search(
                []
            ).read()

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_employers, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error("Error '%s' when fetching all Employers" % e)
            response = Response("[]", content_type="application/json")

        return response
